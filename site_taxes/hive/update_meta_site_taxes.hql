msck repair table raw.site_details_raw;

insert overwrite table meta.site_taxes

select
sd.uuid,
sd.customer.id,
sd.customer.name,
sd.customer.code,
MAX(CASE WHEN taxes.taxtype = 'PST' THEN taxes.rate ELSE ''END) AS pst,
MAX(CASE WHEN taxes.taxtype = 'GST' THEN taxes.rate ELSE ''END) AS gst,
MAX(CASE WHEN taxes.taxtype = 'HST' THEN taxes.rate ELSE ''END) AS hst

from raw.site_details_raw sd

left join (select 
sdr.uuid as uuid,
addr_prov_taxes.rate as rate,
addr_prov_taxes.taxtypeabbr as taxtype
from raw.site_details_raw sdr 
lateral view explode(address.provincetaxes) ap as addr_prov_taxes
  ) taxes on sd.uuid = taxes.uuid 

group by sd.customer.code, sd.customer.name, sd.customer.id, sd.uuid;

msck repair table meta.site_taxes;


#!/usr/bin/env python
# -*- coding: utf-8 -*-
from openpyxl.styles import Font
from openpyxl.utils.dataframe import dataframe_to_rows
import csv
import datetime
import os
from impala.dbapi import connect
import calendar
from itertools import groupby, tee
from operator import itemgetter

from decimal import Decimal
import pandas as pd
from openpyxl import load_workbook
import shutil

template ="template.xlsx"
template_payment_summary ="template_payment_summary.xlsx"

# HADOOP_HOST="172.30.54.174"
# HADOOP_HOST="172.30.136.17"
HADOOP_HOST="172.30.136.57"
# HADOOP_HOST="172.20.22.10"
HADOOP_PORT=21050
HADOOP_LIMIT_MINUTES=20

# DEFAULT_CREDIT_CARD_RATE = {'American Express': 4.23, 'MasterCard': 3.25, 'Visa': 3.25}
# FIXED_FEES = 0.90
# REVENUE_SHARING_RATIO = 50

DEFAULT_CREDIT_CARD_RATE = {'American Express': 4.23, 'MasterCard': 3.25, 'Visa': 3.25}
FIXED_FEES = 0.50
REVENUE_SHARING_RATIO = 50


BASE_SAVE_FOLDER = '/home/mouedrassi/Reconciliation_reports'
# BASE_SAVE_FOLDER = '/home/cronuser/crons/hydra/reconciliation_reports/data'


def get_data_dict_from_hadoop(sql_in):

        host = HADOOP_HOST
        port = HADOOP_PORT

        data_out = []
        # log.debug('sql execute query "{0}" on server {1} port {2}'.format(sql_in, host, port))
        try:
            conn = connect(host, port)
            cur = conn.cursor()
            cur.execute(sql_in)
            columns = [column[0] for column in cur.description]
            for row in cur.fetchall():
                data_out.append(dict(zip(columns, row)))

        except Exception as e:
            # log.exception(e)
            print e
        except RuntimeError as re:
            # log.exception(re)
            print re
        finally:
            conn.close()
        return data_out


class ReconciliationReports(object):

    sql_account = """
      SELECT
       account_details.transaction_number,
       account_details.device_mac,
       account_details.card_type,
       account_details.pkg_name,
       account_details.account_startdate,
       account_details.base_price,
       account_details.gst,
       account_details.pst,
       account_details.hst,
       account_details.purchase_amount,
       account_details.site_uuid,
       account_details.payment_processor,
       account_details.account_date,
       site_details.site_name,
       site_details.dsid,
       site_details.customer_name,
       site_details.customer_code
       FROM
        (select
        COALESCE(account.transaction_number, '') as transaction_number,
        COALESCE(account.device_mac, '') as device_mac,
        COALESCE(account.card_type, '') as card_type,
        COALESCE(account.pkg_name, '') as pkg_name,
        COALESCE(account.account_startdate, '') as account_startdate,
        COALESCE(account.base_price, 0) as base_price,
        COALESCE(account.gst, 0) as gst,
        COALESCE(account.pst, 0) as pst,
        COALESCE(account.hst, 0) as hst,
        COALESCE(account.purchase_amount, 0) as purchase_amount,
        COALESCE(account.site_uuid, '') as site_uuid,
        COALESCE(account.pkg_customerid, 0) as pkg_customerid,
        COALESCE(account.payment_processor, '') as payment_processor,

        to_date(concat(cast(account.year as string),'-', LPAD(cast(account.month as string),2,'0'),'-', LPAD(cast(account.day as string),2,'0')))  as account_date
        from derived.account_details_report account
        where
        to_date(concat(cast(account.year as string),'-', LPAD(cast(account.month as string),2,'0'),'-', LPAD(cast(account.day as string),2,'0')))
        between
        to_date(to_utc_timestamp('{0}', 'Canada/Eastern')) and
        to_date(to_utc_timestamp('{1}', 'Canada/Eastern')))
                                  account_details LEFT JOIN
                                                (select site_uuid, site_name, dsid, customer_name, customer_code from meta.site_details
                                                group by site_uuid, site_name, dsid, customer_name, customer_code
                                                order by customer_name) site_details

        ON account_details.site_uuid = site_details.site_uuid where customer_code = '{2}'
        order by account_startdate;
    """

    sql_fees_ratio = """
    select
    customer.customer_code,
    customer.customer_name,
    customer.fixed_fees,
    customer.revenue_sharing_ratio,
    --customer.package_federal_tax,
    --customer.package_provincial_tax,
    --customer.fees_federal_Tax,
    --customer.fees_provincial_tax,
    to_date(concat(cast(customer.year as string),'-', LPAD(cast(customer.month as string),2,'0'),'-', LPAD(cast(customer.day as string),2,'0'))) as update_date
    from meta.customer_details customer
    where
    to_date(concat(cast(customer.year as string),'-', LPAD(cast(customer.month as string),2,'0'),'-', LPAD(cast(customer.day as string),2,'0')))
    between
    to_date(to_utc_timestamp('{0}', 'Canada/Eastern')) and
    to_date(to_utc_timestamp('{1}', 'Canada/Eastern')) and
    customer_code = '{2}';
    """

    sql_fees_ratio_by_site = """
    select
    site_name,
    site_uuid,
    customer_code,
    customer_name,
    fixed_fees,
    revenue_sharing_ratio
    from  meta.site_details
    where customer_code = '{0}';
        """

    sql_rate = """
    select
    concat(credit_card_type_name,'-', payment_process_name,'-', update_date) as pk,
    rate,
    credit_card_type_name,
    payment_process_name,
    update_date
    from meta.creditcardrate
    where
    update_date
    between
    to_date(to_utc_timestamp('{0}', 'Canada/Eastern')) and
    to_date(to_utc_timestamp('{1}', 'Canada/Eastern'))
    """

    fields_details = ('site_name', 'transaction_number', 'device_mac', 'card_type', 'pkg_name', 'account_date', 'net_sales', 'tax_fed', 'tax_prov', 'purchase_amount')
    fields_payment_summary = ('dsid', 'site_name', 'ratio', 'net_sales')

    fields_summary_card = ('dsid', 'site_name', 'card_type', 'total_trans', 'net_sales', 'tax_fed', 'tax_prov', 'gross_sales',
                            'revenue_share_operator',
                            'revenue_share_location',
                            'transaction_charge_variable',
                            'transaction_charge_fixed',
                            'transaction_charge_total',
                            'net_revenue_operator',
                            'net_revenue_location'
                           )

    def __init__(self, first_date='', last_date='', customer_code='', customer_name=''):

        self.all_data = []
        self.data_details = []
        self.data_account = []
        self.data_fees_ratio = {}
        self.data_fees_ratio_by_site = {}
        self.data_rate = {}
        self.sites = {}

        if first_date:
            self.first_date = datetime.datetime.strptime(first_date, "%Y-%m-%d")
        else:
            self.first_date = datetime.datetime.now().replace(day=1, hour=0, minute=0)
            self.first_date = self.first_date - datetime.timedelta(days=1)
            self.first_date = self.first_date.replace(day=1, hour=0, minute=0)

        if last_date:
            self.last_date = datetime.datetime.strptime(last_date, "%Y-%m-%d")
        else:
            week_day, last_day = calendar.monthrange(self.first_date.year,  self.first_date.month)
            self.last_date = self.first_date.replace(day=last_day, hour=0, minute=0)

        self.customer_code = customer_code
        self.customer_name = customer_name

        self.sql_account = self.sql_account.format(self.first_date, self.last_date, self.customer_code)
        self.sql_fees_ratio_by_site = self.sql_fees_ratio_by_site.format(self.customer_code)
        self.sql_rate = self.sql_rate.format(self.first_date, self.last_date)


        print self.sql_account
        print self.sql_fees_ratio_by_site
        print self.sql_rate

    def get_data_rate(self):
        data_in = get_data_dict_from_hadoop(self.sql_rate)
        data_out = {}
        for data in data_in:
            data_out[data.get('pk', '')] = data.get('rate', '')
        return data_out

    def get_rate_by_card_type_payment_process_date(self, card_type, payment_process, date):
        pk = ''
        if card_type and payment_process and date:
            pk = card_type + '-' + payment_process + '-' + str(date)
        rate = self.data_rate.get(pk, 0)

        if not rate:
            rate = DEFAULT_CREDIT_CARD_RATE.get(card_type, 3.25)

        return rate

    def get_data_fees_ratio_by_site(self):
        data_in = get_data_dict_from_hadoop(self.sql_fees_ratio_by_site)
        data_out = {}
        for data in data_in:
            data_out[data.get('site_uuid', '')] = {'fixed_fees': "%.2f" % data.get('fixed_fees', ''),
                                                     'revenue_sharing_ratio': "%.2f" % data.get('revenue_sharing_ratio', ''),
                                                    }
        print data_out
        return data_out

    def get_summary_card(self):
        grouper_card = itemgetter("dsid", "site_name", "card_type")
        result_card = []
        for key, grp in groupby(sorted(self.all_data, key = grouper_card), grouper_card):
            temp_dict = dict(zip(["dsid", "site_name", "card_type"], key))
            grp_list = list(grp)
            temp_dict["total_trans"] = len(grp_list)

            for field in self.fields_summary_card:
                if field not in ("dsid", "site_name", "card_type", "total_trans"):
                    temp_dict[field] = Decimal('{0:.2f}'.format(sum([item[field] for item in grp_list])))

            result_card.append(temp_dict)

        return result_card

    def get_payment_summary(self):
        grouper_card = itemgetter("dsid", "site_name", "ratio")
        payment_summary = []
        for key, grp in groupby(sorted(self.all_data, key = grouper_card), grouper_card):
            temp_dict = dict(zip(["dsid", "site_name", "ratio"], key))
            grp_list = list(grp)

            for field in self.fields_payment_summary:
                if field not in ("dsid", "site_name", "ratio"):
                    temp_dict[field] = Decimal('{0:.2f}'.format(sum([item[field] for item in grp_list])))

            temp_dict['ratio'] = Decimal('{0:.2f}'.format(Decimal(temp_dict['ratio'])/100))
            print temp_dict
            payment_summary.append(temp_dict)

        return payment_summary

    def data_to_csv(self, path_file_details, data_in, fields):
        with open(path_file_details, 'wb') as output_file:
            dict_writer = csv.DictWriter(output_file, fields)
            dict_writer.writeheader()
            dict_writer.writerows(data_in)

    def create_files_csv(self):
        #Report data_details
        year = self.last_date.year
        month = self.last_date.month
        path_folder = '{0}/{1}/{2}/{3}/'.format(BASE_SAVE_FOLDER, year, month, self.customer_name.replace(" ", "_"))

        details = self.data_details
        summary_card = self.get_summary_card()

        if not os.path.exists(path_folder) and details:
            os.makedirs(path_folder)

        sites = [d['site_name'] for d in details]
        sites = set(sites)

        if details:
            path_file_details = '{0}{1}-Detailed.csv'.format(path_folder, self.customer_name.replace(" ", "_"))
            self.data_to_csv(path_file_details, details, self.fields_details)

        for site in sites:
            detail_by_site = [d for d in details if d['site_name'] in site]
            if detail_by_site:
                path_file_details = '{0}{1}-Detailed.csv'.format(path_folder, site.replace(" ", "_"))
                self.data_to_csv(path_file_details, detail_by_site, self.fields_details)

        # Report summary_card
        if summary_card:
            path_file_details = '{0}{1}-Reconciliation.csv'.format(path_folder, self.customer_name.replace(" ", "_"))
            self.data_to_csv(path_file_details, summary_card, self.fields_summary_card)

    @staticmethod
    def generate_data_xlsx(worksheet, data, report, **kwargs):

        if data.empty:
            return False

        index = kwargs.get('index', False)
        header = kwargs.get('header', True)
        date = kwargs.get('date', '')
        first_row = kwargs.get('first_row', 1)
        first_col = kwargs.get('first_col', 1)

        if report == 'report_tab':
            worksheet.cell(row=2, column=2).value = date

        rows = dataframe_to_rows(data, index=index, header=header)
        for r_idx, row in enumerate(rows, first_row):
            for c_idx, value in enumerate(row, first_col):
                worksheet.cell(row=r_idx, column=c_idx, value=value)

                if report == 'dump_detail':
                    if r_idx == 1:
                        worksheet.cell(row=r_idx, column=c_idx).font = Font(bold=True)
                    if c_idx in (6,):
                        worksheet.cell(row=r_idx, column=c_idx).number_format = 'M/D/YYYY'
                    if c_idx in (7, 8, 9, 10):
                        worksheet.cell(row=r_idx, column=c_idx).number_format = '" $ "#,##0.00 ;" $ ("#,##0.00);" $ -"# ;@'

                if report == 'dump_summary':
                    if r_idx == 1:
                        worksheet.cell(row=r_idx, column=c_idx).font = Font(bold=True)
                    if c_idx in range(1, 2, 3):
                        worksheet.column_dimensions["A"].width = 30
                    if c_idx in range(5, 16):
                        worksheet.cell(row=r_idx, column=c_idx).number_format = '" $ "#,##0.00 ;" $ ("#,##0.00);" $ -"# ;@'

                if report == 'payement_summary':
                    formula = "=C{0}*D{0}".format(r_idx)
                    worksheet.cell(row=r_idx, column=3).number_format = "0%"
                    worksheet.cell(row=r_idx, column=4).number_format = '" $ "#,##0.00 ;" $ ("#,##0.00);" $ -"# ;@'
                    worksheet.cell(row=r_idx, column=5).value = formula
                    worksheet.cell(row=r_idx, column=5).number_format = '" $ "#,##0.00 ;" $ ("#,##0.00);" $ -"# ;@'

    def create_files_xlsx_by_customer_site(self):

        if not self.all_data:
            return False

        year = self.last_date.year
        month = self.last_date.month
        path_folder = '{0}/{1}/{2}/{3}/'.format(BASE_SAVE_FOLDER, year, month, self.customer_name.replace(" ", "_"))

        summary_card = self.get_summary_card()
        payment_summary = self.get_payment_summary()

        df_data_detail = pd.DataFrame(self.data_details)
        df_summary_card = pd.DataFrame(summary_card)
        df_payment_summary = pd.DataFrame(payment_summary)

        sites = df_summary_card[['dsid', 'site_name']].drop_duplicates().sort_values(['site_name'])
        summary = df_payment_summary[list(self.fields_payment_summary)].sort_values(['site_name'])
        dump_summary = df_summary_card[list(self.fields_summary_card)].sort_values(['site_name'])
        dump_detail = df_data_detail[list(self.fields_details)].sort_values(['site_name'])

        if not os.path.exists(path_folder):
            os.makedirs(path_folder)

        path_file_xlsx = '{0}{1}.xlsx'.format(path_folder, self.customer_name.replace(" ", "_"))
        shutil.copy(template, path_file_xlsx)

        wb = load_workbook(path_file_xlsx)

        payment_summary_name = 'Payement Summary'
        payment_summary_sheet = wb[payment_summary_name]
        self.generate_data_xlsx(payment_summary_sheet, summary, report='payement_summary', first_row=2, index=False, header=False)

        data_summary_name = 'Data dump summary'
        data_summary_sheet = wb[data_summary_name]
        self.generate_data_xlsx(data_summary_sheet, dump_summary, report='dump_summary', first_row=1, index=False, header=True)

        data_detail_name = 'Data dump detail'
        data_detail_sheet = wb[data_detail_name]
        self.generate_data_xlsx(data_detail_sheet, dump_detail, report='dump_detail', first_row=1, index=False, header=True)

        source_sheet = wb['base']

        for index, row in sites.iterrows():
            target = wb.copy_worksheet(source_sheet)

            site_name = row['site_name']
            dsid = row['dsid']
            site_name = unicode(site_name, "utf-8")
            target.title = site_name
            current_sheet = wb[site_name]
            summary_by_site = df_summary_card[list(self.fields_summary_card)][df_summary_card['dsid']==dsid]
            self.generate_data_xlsx(current_sheet, summary_by_site, report='report_tab', date=self.first_date, first_row=9, index=False, header=False)

        wb.remove(source_sheet)
        wb.save(path_file_xlsx)

    def fetch_data(self):

        self.data_account = get_data_dict_from_hadoop(self.sql_account)
        # self.data_fees_ratio = self.get_data_fees_ratio()
        self.data_fees_ratio_by_site = self.get_data_fees_ratio_by_site()
        self.data_rate = self.get_data_rate()

        for account in self.data_account:

            site_uuid = account.get('site_uuid', '')

            fixed_fees_ratio = self.data_fees_ratio_by_site.get(site_uuid, {})


            fixed_fees = float(fixed_fees_ratio.get('fixed_fees', 0))
            ratio = float(fixed_fees_ratio.get('revenue_sharing_ratio', 0))

            try:
                date_time = datetime.datetime.strptime(account.get('account_date'), "%Y-%m-%d")
                date_tmp = date_time.date()
            except:
                date_tmp = ''

            account['account_date'] = date_tmp

            rate = self.get_rate_by_card_type_payment_process_date(account.get('card_type'), account.get('payment_process'), date_tmp)

            if not fixed_fees:
                fixed_fees = FIXED_FEES
            account['fixed_fees'] = fixed_fees

            if not ratio:
                ratio = REVENUE_SHARING_RATIO
            account['ratio'] = ratio

            account['revenue_share_location'] = account['base_price'] * ratio/100
            account['revenue_share_operator'] = account['base_price'] - account['revenue_share_location']

            account['transaction_charge_variable'] = account['purchase_amount'] * rate/100
            account['transaction_charge_fixed'] = fixed_fees
            account['transaction_charge_total'] = account['transaction_charge_variable'] + account['transaction_charge_fixed']

            account['net_revenue_operator'] = account['revenue_share_operator'] + account['transaction_charge_total']
            account['net_revenue_location'] = account['revenue_share_location'] - account['transaction_charge_total']

            # ****************//
            account['net_sales'] = account['base_price']
            account['tax_fed'] = account['gst'] + account['hst']
            account['tax_prov'] = account['pst']
            account["gross_sales"] = account['purchase_amount']

            #add data
            self.all_data.append(account)

            #Custom data
            data = dict((k, account[k]) for k in self.fields_details)
            self.data_details.append(data)

        # print 'self.data_taxes = > ', self.data_taxes
        print 'self.data_rate = > ', self.data_rate
        print 'DEFAULT_CREDIT_CARD_RATE => ', DEFAULT_CREDIT_CARD_RATE

        print 'Customer Name : ', self.customer_code, '|', self.customer_name
        print 'FIXED_FEES : ', FIXED_FEES, '|',\
              'REVENUE_SHARING_RATIO : ', REVENUE_SHARING_RATIO, '|'

        print '-----------------------self.all_data-------------------'
        for a in self.all_data:
            print a

        print '-----------------------data_details-------------------'
        for a in self.data_details:
            print a

        print '-----------------------summary_card-------------------'
        for a in self.get_summary_card():
            print a


if __name__ == "__main__":

    # rec = ReconciliationReports(first_date='2017-04-01', last_date='2017-04-30', customer_code='BCS',  customer_name='BC Health')
    # rec = ReconciliationReports(customer_code='CHQ',  customer_name='CHUQ')
    rec = ReconciliationReports(customer_code='BCS',  customer_name='BC Health')
    rec.fetch_data()
    # rec.create_files_csv()
    rec.create_files_xlsx_by_customer_site()


    # sql_customer = """
    #       select distinct customer_name, customer_code
    #       from meta.site_details
    #       order by customer_name, customer_code
    #     """
    #
    # customers = get_data_dict_from_hadoop(sql_customer)
    #
    # # for customer in customers:
    # #      print customer.get('customer_code'), '___________',  customer.get('customer_name')
    #
    #
    # for customer in customers:
    #     print customer.get('customer_code'), '***************',customer.get('customer_name')
    #     # rec = ReconciliationReports(first_date='2017-04-01', last_date='2017-04-30', customer_code=customer.get('customer_code'),  customer_name = customer.get('customer_name'))
    #     rec = ReconciliationReports(customer_code=customer.get('customer_code'),  customer_name = customer.get('customer_name'))
    #     rec.fetch_data()
    #     rec.create_files_csv()
    #     rec.create_files_xlsx_by_customer_site()
    #








#!/usr/bin/env python
# -*- coding: utf-8 -*-
from impala.util import as_pandas
import openpyxl
from openpyxl.styles import Font
from openpyxl.utils.dataframe import dataframe_to_rows
import datetime
import os
from impala.dbapi import connect
import calendar
import pandas as pd
from openpyxl import load_workbook
import shutil


template ="/home/cronuser/crons/hydra/reconciliation_reports/template_paypal.xlsx"
datavalet_logo ="/home/cronuser/crons/hydra/reconciliation_reports/Datavalet.png"
# template_payment_summary ="template_payment_summary.xlsx"

HADOOP_HOST="172.20.22.25"
HADOOP_PORT="21050"
HADOOP_LIMIT_MINUTES=20

DEFAULT_CREDIT_CARD_RATE = {'American Express': 5.00, 'MasterCard': 5.00, 'Visa': 5.00}
# DEFAULT_CREDIT_CARD_RATE = {'American Express': 4.23, 'MasterCard': 3.25, 'Visa': 3.25}
DEFAULT_PAYPAL_RATE = 5.00
# DEFAULT_PAYPAL_RATE = 3.25

CREDIT_CARD_NAME = {'American Express': 'AX', 'MasterCard': 'MC', 'Visa': 'VI', 'Paypal': 'Paypal'}


def get_default_taxs_fees_by_customer(customer_code):

    FIXED_FEES = 0.50
    REVENUE_SHARING_RATIO = 100.00
    PACKAGE_FEDERAL_TAX = 5.00
    PACKAGE_PROVINCIAL_TAX = 7.00
    FEES_FEDERAL_TAX = 5.00
    FEES_PROVINCIAL_TAX = 0.00

    if customer_code == 'BCS':
        # BC Health
        FIXED_FEES = 0.50
        REVENUE_SHARING_RATIO = 100.00
        PACKAGE_FEDERAL_TAX = 5.00
        PACKAGE_PROVINCIAL_TAX = 7.00
        FEES_FEDERAL_TAX = 5.00
        FEES_PROVINCIAL_TAX = 0.00

    if customer_code == 'GBH':
        # Grey Bruce Hospital
        FIXED_FEES = 0.90
        REVENUE_SHARING_RATIO = 30.00
        PACKAGE_FEDERAL_TAX = 13.00
        PACKAGE_PROVINCIAL_TAX = 0.00
        FEES_FEDERAL_TAX = 13.00
        FEES_PROVINCIAL_TAX = 0.00

    if customer_code == 'CHQ':
        # CHQ
        FIXED_FEES = 0.90
        REVENUE_SHARING_RATIO = 100.00
        PACKAGE_FEDERAL_TAX = 5.00
        PACKAGE_PROVINCIAL_TAX = 9.975
        FEES_FEDERAL_TAX = 5.00
        FEES_PROVINCIAL_TAX = 9.975

    if customer_code in ('OMH' , 'MDH', 'TMH', 'AHI', 'MKH','BCH','EGC'):
        # 'OMH' , 'MDH', 'TMH', 'AHI', 'MKH'
        FIXED_FEES = 0.90
        REVENUE_SHARING_RATIO = 100.00
        PACKAGE_FEDERAL_TAX = 13.00
        PACKAGE_PROVINCIAL_TAX = 0.00
        FEES_FEDERAL_TAX = 13.00
        FEES_PROVINCIAL_TAX = 0.00

    return FIXED_FEES, REVENUE_SHARING_RATIO, PACKAGE_FEDERAL_TAX, PACKAGE_PROVINCIAL_TAX, FEES_FEDERAL_TAX, FEES_PROVINCIAL_TAX


#BASE_SAVE_FOLDER = '/home/mouedrassi/Reconciliation_reports'
BASE_SAVE_FOLDER = '/home/cronuser/crons/hydra/reconciliation_reports/data'


def get_df_data_from_hadoop(sql_in):

        host = HADOOP_HOST
        port = HADOOP_PORT
        df = pd.DataFrame()
        try:
            conn = connect(host,port)
            cur = conn.cursor()
            cur.execute(sql_in)
            df = as_pandas(cur)
        except Exception as e:
            # log.exception(e)
            print(e)
        except RuntimeError as re:
            # log.exception(re)
            print(re)
        finally:
            conn.close()
        return df


def get_data_dict_from_hadoop(sql_in):

        host = HADOOP_HOST
        port = HADOOP_PORT

        data_out = []
        # log.debug('sql execute query "{0}" on server {1} port {2}'.format(sql_in, host, port))
        try:
            conn = connect(host,port)
            cur = conn.cursor()
            cur.execute(sql_in)
            columns = [column[0] for column in cur.description]
            for row in cur.fetchall():
                data_out.append(dict(zip(columns, row)))

        except Exception as e:
            # log.exception(e)
            print(e)
        except RuntimeError as re:
            # log.exception(re)
            print(re)
        finally:
            conn.close()
        return data_out

sql_account = """
      SELECT
       account_details.transaction_number,
       account_details.device_mac,
       account_details.card_type,
       account_details.pkg_name,
       account_details.account_startdate,
       account_details.base_price,
       account_details.gst,
       account_details.pst,
       account_details.hst,
       account_details.donationamount,
       account_details.purchase_amount,
       account_details.site_uuid,
       account_details.payment_processor,
       account_details.account_date,
       site_details.site_name,
       site_details.dsid,
       site_details.customer_name,
       site_details.customer_code
       FROM
        (select
        COALESCE(account.transaction_number, '') as transaction_number,
        COALESCE(account.device_mac, '') as device_mac,
        COALESCE(account.card_type, '') as card_type,
        COALESCE(account.pkg_name, '') as pkg_name,
        COALESCE(account.account_startdate, '') as account_startdate,
        COALESCE(account.base_price, 0) as base_price,
        COALESCE(account.gst, 0) as gst,
        COALESCE(account.pst, 0) as pst,
        COALESCE(account.hst, 0) as hst,
        COALESCE(account.donationamount, 0) as donationamount,
        COALESCE(account.purchase_amount, 0) as purchase_amount,
        COALESCE(account.site_uuid, '') as site_uuid,
        COALESCE(account.pkg_customerid, 0) as pkg_customerid,
        COALESCE(account.payment_processor, '') as payment_processor,

        to_date(concat(cast(account.year as string),'-', LPAD(cast(account.month as string),2,'0'),'-', LPAD(cast(account.day as string),2,'0')))  as account_date
        from derived.account_details_report account
        where
        to_date(concat(cast(account.year as string),'-', LPAD(cast(account.month as string),2,'0'),'-', LPAD(cast(account.day as string),2,'0')))
        between
        to_date(to_utc_timestamp('{0}', 'Canada/Eastern')) and
        to_date(to_utc_timestamp('{1}', 'Canada/Eastern')))
                                  account_details LEFT JOIN
                                                (select site_uuid, site_name, dsid, customer_name, customer_code from meta.site_details
                                                group by site_uuid, site_name, dsid, customer_name, customer_code
                                                order by customer_name) site_details

        ON account_details.site_uuid = site_details.site_uuid where customer_code = '{2}'
        order by account_startdate;
    """

sql_fees_ratio_tax = """
    select
    dsid,
    site_name,
    site_uuid,
    customer_code,
    customer_name,
    ISNULL(fixed_fees, 0) as fixed_fees,
    ISNULL(revenue_sharing_ratio, 0) as revenue_sharing_ratio,
    ISNULL(package_federal_tax, 0) as package_federal_tax,
    ISNULL(package_provincial_tax, 0) as package_provincial_tax,
    ISNULL(fees_federal_tax, 0) as fees_federal_tax,
    ISNULL(fees_provincial_tax, 0) as fees_provincial_tax
    from  meta.site_details
    where customer_code = '{0}';
        """

sql_package_tax = """
    select
    site_uuid,
    customer_id,
    customer_name,
    customer_code,
    pst,
    gst,
    hst
    from meta.site_taxes
    where customer_code = '{0}';
        """

sql_rate = """
    select
    concat(credit_card_type_name,'-', payment_process_name,'-', update_date) as pk,
    rate,
    credit_card_type_name,
    payment_process_name,
    update_date
    from meta.creditcardrate
    where
    update_date
    between
    to_date(to_utc_timestamp('{0}', 'Canada/Eastern')) and
    to_date(to_utc_timestamp('{1}', 'Canada/Eastern'))
    """


class ReconciliationReports(object):

    fields_details = ('site_name', 'transaction_number', 'device_mac', 'card_type', 'pkg_name', 'account_date',
                      'net_sales', 'donation', 'tax_fed', 'tax_prov', 'purchase_amount')

    fields_summary_customer_report = ('site_name', 'net_sales', 'donation', 'taxes_on_sales', 'gross_sales', 'transaction_fees',
                                      'taxes_on_transaction_fees', 'net_payable_to_customer')

    fields_summary_customer_revenue_share = ('dsid', 'site_name', 'revenue_share', 'gross_sales', 'charges', 'net_payable_to_customer')

    fields_summary_card = ('dsid',
                           'site_name',
                           'card_type',
                           'total_trans',
                           'net_sales',
                           'donation',
                           'tax_fed',
                           'tax_prov',
                           'gross_sales',
                           'revenue_share_operator',
                           'revenue_share_location',
                           'transaction_charge_variable',
                           'transaction_charge_fixed',
                           'transaction_charge_total',
                           'net_revenue_operator',
                           'net_revenue_location')

    fields_taxes = ('fixed_fees',
                    'ratio',
                    'package_federal_tax',
                    'package_provincial_tax',
                    'fees_federal_tax',
                    'fees_provincial_tax')

    fields_by_card_type = ('card_type',
                           'total_trans',
                           'net_sales',
                           'donation',
                           'tax_fed',
                           'tax_prov',
                           'gross_sales',
                           'revenue_share_operator',
                           'revenue_share_location',
                           'transaction_charge_variable',
                           'transaction_charge_fixed',
                           'transaction_charge_total',
                           'net_revenue_operator',
                           'net_revenue_location',
                           'dsid',
                           'site_name',)

    fields_by_card_type_taxes = fields_by_card_type + fields_taxes

    def __init__(self, first_date='', last_date='', customer_code='', customer_name=''):

        self.df_account = pd.DataFrame()
        self.df_data_details = pd.DataFrame()
        self.df_sites = pd.DataFrame()
        self.df_summary_card = pd.DataFrame()
        self.df_summary_customer = pd.DataFrame()
        self.df_summary_customer_revenue_share = pd.DataFrame()

        if first_date:
            self.first_date = datetime.datetime.strptime(first_date, "%Y-%m-%d")
        else:
            self.first_date = datetime.datetime.now().replace(day=1, hour=0, minute=0)
            self.first_date = self.first_date - datetime.timedelta(days=1)
            self.first_date = self.first_date.replace(day=1, hour=0, minute=0)

        if last_date:
            self.last_date = datetime.datetime.strptime(last_date, "%Y-%m-%d")
        else:
            week_day, last_day = calendar.monthrange(self.first_date.year,  self.first_date.month)
            self.last_date = self.first_date.replace(day=last_day, hour=0, minute=0)

        self.customer_code = customer_code
        self.customer_name = customer_name

        self.sql_account = sql_account.format(self.first_date, self.last_date, self.customer_code)
        self.sql_fees_ratio_tax = sql_fees_ratio_tax.format(self.customer_code)
        self.sql_package_tax = sql_package_tax.format(self.customer_code)
        self.sql_rate = sql_rate.format(self.first_date, self.last_date)

        print(self.sql_account)
        print(self.sql_fees_ratio_tax)
        print(self.sql_package_tax)
        print(self.sql_rate)

    def adjusted_width_column(self, worksheet):
        for col in worksheet.columns:
             max_length = 0
             column = col[0].column # Get the column name
             for cell in col:
                 try: # Necessary to avoid error on empty cells
                     if len(str(cell.value)) > max_length:
                         max_length = len(cell.value)
                 except:
                     pass
             # adjusted_width = (max_length + 2) * 1.2
             adjusted_width = max_length + 2
             worksheet.column_dimensions[column].width = adjusted_width

    def generate_data_xlsx(self, worksheet, data, report, **kwargs):

        if data.empty:
            return False

        index = kwargs.get('index', False)
        header = kwargs.get('header', True)
        first_row = kwargs.get('first_row', 1)
        first_col = kwargs.get('first_col', 1)

        rows = dataframe_to_rows(data, index=index, header=header)
        for r_idx, row in enumerate(rows, first_row):

            for c_idx, value in enumerate(row, first_col):
                worksheet.cell(row=r_idx, column=c_idx, value=value)
                worksheet.cell(row=1, column=c_idx).font = Font(bold=True)

                if report == 'dump_detail':
                    worksheet.cell(row=r_idx, column=6).number_format = 'M/D/YYYY'
                    for col in range(7, 11):
                        worksheet.cell(row=r_idx, column=col).number_format = '" $ "#,##0.00 ;" $ ("#,##0.00);" $ -"# ;@'

                if report == 'dump_summary':
                    worksheet.column_dimensions["A"].width = 20
                    for col in range(5, 16):
                        worksheet.cell(row=r_idx, column=col).number_format = '" $ "#,##0.00 ;" $ ("#,##0.00);" $ -"# ;@'

                if report == 'payement_summary':
                    # worksheet.cell(row=r_idx, column=3).number_format = "0%"
                    # formula = "=C{0}*D{0}".format(r_idx)
                    # worksheet.cell(row=r_idx, column=5).value = formula
                    worksheet.cell(row=r_idx, column=3).number_format = "0%"
                    for col in range(4, 7):
                        worksheet.cell(row=r_idx, column=col).number_format = '" $ "#,##0.00 ;" $ ("#,##0.00);" $ -"# ;@'

                if report == 'summary_customer':
                    for col in range(2, 9):
                        worksheet.cell(row=r_idx, column=col).number_format = '" $ "#,##0.00 ;" $ ("#,##0.00);" $ -"# ;@'

        self.adjusted_width_column(worksheet)

    def generate_data_xlsx_tab(self, worksheet, dsid, **kwargs):

        data = self.df_data_by_card_type[self.df_data_by_card_type['dsid']==dsid].reset_index(drop=True)
        if data.empty:
            return False

        index = kwargs.get('index', False)
        header = kwargs.get('header', True)
        date = kwargs.get('date', '')
        first_row = kwargs.get('first_row', 1)
        first_col = kwargs.get('first_col', 1)

        # dsid_val = data.ix[0, 'dsid']
        site_name = data.ix[0, 'site_name']

        worksheet['K2'] = date
        worksheet['K3'] = site_name
        worksheet['I19'] = site_name
        worksheet['K4'] = data.ix[0, 'dsid']

        img = openpyxl.drawing.image.Image(datavalet_logo)
        # img = openpyxl.drawing.image.Image('Datavalet.jpg')
        worksheet.add_image(img, 'B2')

        worksheet['G37'] = self.df_taxes.loc[dsid, 'fixed_fees']
        worksheet['G38'] = self.df_taxes.loc[dsid, 'ratio']
        worksheet['G39'] = self.df_taxes.loc[dsid, 'package_federal_tax']
        worksheet['G40'] = self.df_taxes.loc[dsid, 'package_provincial_tax']
        worksheet['G41'] = self.df_taxes.loc[dsid, 'fees_federal_tax']
        worksheet['G42'] = self.df_taxes.loc[dsid, 'fees_provincial_tax']

        worksheet['G38'].number_format = "0.###%"
        worksheet['G39'].number_format = "0.###%"
        worksheet['G40'].number_format = "0.###%"
        worksheet['G41'].number_format = "0.###%"
        worksheet['G42'].number_format = "0.###%"

        rows = dataframe_to_rows(data, index=index, header=header)
        for r_idx, row in enumerate(rows, first_row):
            for c_idx, value in enumerate(row, first_col):
                if c_idx == 2:
                    worksheet.cell(row=r_idx, column=c_idx, value=CREDIT_CARD_NAME.get(value, ''))
                else:
                    if c_idx < 16:
                        worksheet.cell(row=r_idx, column=c_idx, value=value)

    def create_files_xlsx_by_customer_site(self):

        if self.df_account.empty:
            return False

        year = self.last_date.year
        month = self.last_date.month
        path_folder = '{0}/{1}/{2}/{3}/'.format(BASE_SAVE_FOLDER, year, month, self.customer_name.replace(" ", "_"))

        if not os.path.exists(path_folder):
            os.makedirs(path_folder)

        path_file_xlsx = '{0}{1}.xlsx'.format(path_folder, self.customer_name.replace(" ", "_"))
        shutil.copy(template, path_file_xlsx)

        wb = load_workbook(path_file_xlsx)

        payment_summary_name = 'Payment Summary'
        payment_summary_sheet = wb[payment_summary_name]
        self.generate_data_xlsx(payment_summary_sheet, self.df_summary_customer_revenue_share, report='payement_summary', first_row=1, index=False, header=True)

        summary_customer_name = 'Summary customer'
        summary_customer_sheet = wb[summary_customer_name]
        self.generate_data_xlsx(summary_customer_sheet, self.df_summary_customer, report='summary_customer', first_row=1, index=False, header=True)

        data_summary_name = 'Data dump summary'
        data_summary_sheet = wb[data_summary_name]
        self.generate_data_xlsx(data_summary_sheet, self.df_summary_card, report='dump_summary', first_row=1, index=False, header=True)

        data_detail_name = 'Data dump detail'
        data_detail_sheet = wb[data_detail_name]
        self.generate_data_xlsx(data_detail_sheet, self.df_data_details, report='dump_detail', first_row=1, index=False, header=True)

        source_sheet = wb['base_tmp']

        for index, row in self.df_sites.iterrows():
            target = wb.copy_worksheet(source_sheet)

            site_name = row['site_name']
            site_name = site_name.replace('\\','').replace('/','').replace('[','').replace(']','').replace('*','').replace('?','').replace(':','')
            dsid = row['dsid']
            site_name = unicode(site_name, "utf-8")
            # site_name = "test site name"
            target.title = site_name
            current_sheet = wb[site_name]
            self.generate_data_xlsx_tab(current_sheet, dsid, date=self.first_date, first_row=11, first_col=2, index=False, header=False)

        wb.remove(source_sheet)
        wb.save(path_file_xlsx)

    def get_df_data(self):

        self.df_account = get_df_data_from_hadoop(self.sql_account)
        self.df_fees_ratio_tax = get_df_data_from_hadoop(self.sql_fees_ratio_tax)
        self.df_rate = get_df_data_from_hadoop(self.sql_rate)
        self.df_package_tax = get_df_data_from_hadoop(self.sql_package_tax)
        if not self.df_package_tax.empty :
            self.get_package_taxes()

        list_account_date = []
        list_fixed_fees = []
        list_ratio = []
        list_package_federal_tax = []
        list_package_provincial_tax = []
        list_fees_federal_tax = []
        list_fees_provincial_tax = []

        list_revenue_share_location = []
        list_revenue_share_operator = []
        list_transaction_charge_variable = []
        list_transaction_charge_fixed = []
        list_transaction_charge_total = []
        list_net_revenue_operator = []
        list_net_revenue_location = []

        list_net_sales = []
        list_donation = []
        list_tax_fed = []
        list_tax_prov = []
        list_gross_sales = []

        list_rate = []
        list_site_name = []

        for index, row in self.df_account.iterrows():

            # get taxes
            fees_ratio_tax = pd.DataFrame()
            if not self.df_fees_ratio_tax.empty:
                fees_ratio_tax = self.df_fees_ratio_tax.loc[self.df_fees_ratio_tax['dsid'] == row['dsid']]
                if fees_ratio_tax['fixed_fees'].iloc[0] + \
                    fees_ratio_tax['revenue_sharing_ratio'].iloc[0] + \
                    fees_ratio_tax['fees_federal_tax'].iloc[0] + \
                    fees_ratio_tax['fees_provincial_tax'].iloc[0] == 0:
                    fees_ratio_tax = pd.DataFrame()

            fixed_fees = FIXED_FEES if fees_ratio_tax.empty else fees_ratio_tax['fixed_fees'].iloc[0]
            revenue_sharing_ratio = (REVENUE_SHARING_RATIO if fees_ratio_tax.empty else fees_ratio_tax['revenue_sharing_ratio'].iloc[0])/100
            # package_federal_tax = (PACKAGE_FEDERAL_TAX if fees_ratio_tax.empty else fees_ratio_tax['package_federal_tax'].iloc[0])/100
            # package_provincial_tax = (PACKAGE_PROVINCIAL_TAX if fees_ratio_tax.empty else fees_ratio_tax['package_provincial_tax'].iloc[0])/100
            fees_federal_tax = (FEES_FEDERAL_TAX if fees_ratio_tax.empty else fees_ratio_tax['fees_federal_tax'].iloc[0])/100
            fees_provincial_tax = (FEES_PROVINCIAL_TAX if fees_ratio_tax.empty else fees_ratio_tax['fees_provincial_tax'].iloc[0])/100

            package_tax = pd.DataFrame()
            if not self.df_package_tax.empty:
                package_tax = self.df_package_tax.loc[self.df_package_tax['site_uuid'] == row['site_uuid']]

            package_federal_tax = (PACKAGE_FEDERAL_TAX if package_tax.empty else package_tax['package_federal_tax'].iloc[0])
            package_provincial_tax = (PACKAGE_PROVINCIAL_TAX if package_tax.empty else package_tax['package_provincial_tax'].iloc[0])

            # get rate
            try:
                date_time = datetime.datetime.strptime(row['account_date'], "%Y-%m-%d")
                date_tmp = date_time.date()
            except:
                date_tmp = ''

            pk = row['card_type'] + '-' + row['payment_processor'] + '-' + str(date_tmp)

            credit_card_rate = pd.DataFrame
            if not self.df_rate.empty:
                credit_card_rate = self.df_rate.loc[self.df_rate['pk'] == pk]

            rate = (DEFAULT_CREDIT_CARD_RATE.get(row['card_type'], DEFAULT_PAYPAL_RATE) if credit_card_rate.empty else credit_card_rate['rate'].iloc[0])/100


            list_rate.append(rate)
            list_account_date.append(date_tmp)
            list_fixed_fees.append(fixed_fees)
            list_ratio.append(revenue_sharing_ratio)
            list_package_federal_tax.append(package_federal_tax)
            list_package_provincial_tax.append(package_provincial_tax)
            list_fees_federal_tax.append(fees_federal_tax)
            list_fees_provincial_tax.append(fees_provincial_tax)

            revenue_share_location = row['base_price'] * revenue_sharing_ratio
            list_revenue_share_location.append(revenue_share_location)

            revenue_share_operator = row['base_price'] - revenue_share_location
            list_revenue_share_operator.append(revenue_share_operator)

            transaction_charge_variable = row['purchase_amount'] * rate
            list_transaction_charge_variable.append(transaction_charge_variable)

            list_transaction_charge_fixed.append(fixed_fees)

            transaction_charge_total = transaction_charge_variable + fixed_fees
            list_transaction_charge_total.append(transaction_charge_total)

            net_revenue_operator = revenue_share_operator + transaction_charge_total
            list_net_revenue_operator.append(net_revenue_operator)

            net_revenue_location = revenue_share_location - transaction_charge_total
            list_net_revenue_location.append(net_revenue_location)

            list_net_sales.append(row['base_price'])
            list_donation.append(row['donationamount'])
            list_tax_fed.append(row['gst'] + row['hst'])
            list_tax_prov.append(row['pst'])
            list_gross_sales.append(row['purchase_amount'])

            site_name = row['site_name'] if row['site_name'] else row['dsid']
            list_site_name.append(site_name)

        self.df_account['account_date'] = list_account_date
        self.df_account['fixed_fees'] = list_fixed_fees
        self.df_account['ratio'] = list_ratio
        self.df_account['package_federal_tax'] = list_package_federal_tax
        self.df_account['package_provincial_tax'] = list_package_provincial_tax
        self.df_account['fees_federal_tax'] = list_fees_federal_tax
        self.df_account['fees_provincial_tax'] = list_fees_provincial_tax
        self.df_account['rate'] = list_rate

        self.df_account['revenue_share_location'] = list_revenue_share_location
        self.df_account['revenue_share_operator'] = list_revenue_share_operator
        self.df_account['transaction_charge_variable'] = list_transaction_charge_variable
        self.df_account['transaction_charge_fixed'] = list_transaction_charge_fixed
        self.df_account['transaction_charge_total'] = list_transaction_charge_total
        self.df_account['net_revenue_operator'] = list_net_revenue_operator
        self.df_account['net_revenue_location'] = list_net_revenue_location

        self.df_account['net_sales'] = list_net_sales
        self.df_account['donation'] = list_donation
        self.df_account['tax_fed'] = list_tax_fed
        self.df_account['tax_prov'] = list_tax_prov
        self.df_account['gross_sales'] = list_gross_sales

        self.df_account['site_name'] = list_site_name
        self.df_account['total_trans'] = 1

        # All data (raw data)
        self.df_account = self.df_account.sort_values(['site_name', 'card_type']).reset_index(drop=True)
        # self.df_account = self.df_account.round(2).sort_values(['site_name', 'card_type']).reset_index(drop=True)
        # data details
        self.df_data_details = self.df_account[list(self.fields_details)].sort_values(['site_name', 'card_type']).reset_index(drop=True)
         # sites
        self.df_sites = self.df_account[['dsid', 'site_name']].drop_duplicates().sort_values(['site_name']).reset_index(drop=True)
        # summary card
        self.get_summary_card()
        # summary customer
        self.get_summary_customer()
        # taxes
        self.get_taxes()

        self.get_data_by_card_type()
        self.get_data_by_site()

        #print self.df_account.to_string()
        #print self.df_data_by_card_type.to_string()
        #print self.df_data_by_card_type.index
        #print self.df_data_by_site.to_string()
        #print self.df_data_by_site.index
        # print self.df_data_by_site.loc['BCS-BC-POR-77293', 'net_sales']
        # print self.df_new_summary_card.ix[0, 'dsid']
        # print self.df_taxes.to_string()

    def get_summary_card(self):

        fields = dict((k,'sum') for k in self.fields_summary_card if k not in ('dsid', 'site_name' ,'card_type', 'total_trans'))
        fields['total_trans'] = 'count'
        # fields_taxes = dict((k,'first') for k in self.fields_taxes)
        # fields.update(fields_taxes)

        self.df_summary_card = self.df_account.groupby(['dsid', 'site_name' ,'card_type']).agg(fields).reset_index()
        self.df_summary_card = self.df_summary_card[list(self.fields_summary_card)].sort_values(['site_name', 'card_type'])

        # columns_names = dict(zip(self.fields_summary_card, self.fields_summary_card_name))
        # self.df_summary_card.rename(columns=columns_names, inplace=True)

    def get_data_by_card_type(self):
        # columns_names = dict(zip(self.fields_summary_card, self.fields_summary_card_name))
        # self.df_new_summary_card = self.df_summary_card.rename(columns=columns_names, inplace=False)
        # self.df_new_summary_card = self.df_summary_card[list(self.fields_new_summary_card)].sort_values(['site_name', 'card_type'])
        fields = dict((k,'sum') for k in self.fields_summary_card if k not in ('dsid', 'site_name' ,'card_type', 'total_trans'))
        fields['total_trans'] = 'count'
        fields_taxes = dict((k,'first') for k in self.fields_taxes)
        fields.update(fields_taxes)

        data_tmp = self.df_account.groupby(['dsid', 'site_name' ,'card_type']).agg(fields).sort_values(['site_name', 'card_type']).reset_index()
        self.df_data_by_card_type = data_tmp[list(self.fields_by_card_type_taxes)]

    def get_data_by_site(self):

        fields_sum = ['net_sales', 'tax_fed', 'tax_prov', 'gross_sales', 'revenue_share_operator', 'transaction_charge_total']

        fields = dict((k,'sum') for k in fields_sum)
        fields_taxes = dict((k,'first') for k in self.fields_taxes)

        fields.update(fields_taxes)
        data_tmp = self.df_account.groupby(['dsid', 'site_name']).agg(fields)

        taxes_on_sales = data_tmp['tax_fed'] + data_tmp['tax_prov']
        transaction_fees = data_tmp['revenue_share_operator'] + data_tmp['transaction_charge_total']

        data_tmp['taxes_on_sales'] = taxes_on_sales
        data_tmp['transaction_fees'] = transaction_fees

        taxes_on_transaction_fees = (data_tmp['revenue_share_operator'] * data_tmp['package_federal_tax']) + \
                                    (data_tmp['revenue_share_operator'] * data_tmp['package_provincial_tax']) + \
                                    (data_tmp['transaction_charge_total'] * data_tmp['fees_federal_tax']) + \
                                    (data_tmp['transaction_charge_total'] * data_tmp['fees_provincial_tax'])

        data_tmp['taxes_on_transaction_fees'] = taxes_on_transaction_fees

        net_payable_to_customer = (data_tmp['net_sales'] - transaction_fees) + (taxes_on_sales - taxes_on_transaction_fees)
        data_tmp['net_payable_to_customer'] = net_payable_to_customer

        data_tmp['revenue_share'] = data_tmp['ratio']

        charges = transaction_fees + taxes_on_transaction_fees
        gross_sales = data_tmp['net_sales'] + taxes_on_sales

        data_tmp['gross_sales'] = gross_sales
        data_tmp['charges'] = charges

        # data_tmp = data_tmp.round(2)

        self.df_data_by_site = data_tmp.reset_index(level=1, drop=True)

    def get_summary_customer(self):

        fields_sum = ['net_sales', 'donation', 'tax_fed', 'tax_prov', 'gross_sales', 'revenue_share_operator', 'transaction_charge_total']

        fields = dict((k,'sum') for k in fields_sum)
        fields_taxes = dict((k,'first') for k in self.fields_taxes)

        fields.update(fields_taxes)
        data_tmp = self.df_account.groupby(['dsid', 'site_name']).agg(fields).reset_index()

        taxes_on_sales = data_tmp['tax_fed'] + data_tmp['tax_prov']
        transaction_fees = data_tmp['revenue_share_operator'] + data_tmp['transaction_charge_total']

        data_tmp['taxes_on_sales'] = taxes_on_sales
        data_tmp['transaction_fees'] = transaction_fees

        taxes_on_transaction_fees = (data_tmp['revenue_share_operator'] * data_tmp['package_federal_tax']) + \
                                    (data_tmp['revenue_share_operator'] * data_tmp['package_provincial_tax']) + \
                                    (data_tmp['transaction_charge_total'] * data_tmp['fees_federal_tax']) + \
                                    (data_tmp['transaction_charge_total'] * data_tmp['fees_provincial_tax'])

        data_tmp['taxes_on_transaction_fees'] = taxes_on_transaction_fees

        net_payable_to_customer = (data_tmp['net_sales'] + data_tmp['donation'] - transaction_fees) + (taxes_on_sales - taxes_on_transaction_fees)
        data_tmp['net_payable_to_customer'] = net_payable_to_customer

        data_tmp['revenue_share'] = data_tmp['ratio']

        charges = transaction_fees + taxes_on_transaction_fees
        gross_sales = data_tmp['net_sales'] + data_tmp['donation'] + taxes_on_sales

        data_tmp['gross_sales'] = gross_sales
        data_tmp['charges'] = charges

        # data_tmp = data_tmp.round(2)

        self.df_summary_customer = data_tmp[list(self.fields_summary_customer_report)].sort_values(['site_name'])
        self.df_summary_customer = self.df_summary_customer.append(self.df_summary_customer.sum(numeric_only=True), ignore_index=True)

        # Payment Summary
        self.df_summary_customer_revenue_share = data_tmp[list(self.fields_summary_customer_revenue_share)].sort_values(['site_name']).reset_index(drop=True)
        total = self.df_summary_customer_revenue_share.sum(numeric_only=True)
        total['revenue_share'] = ''
        self.df_summary_customer_revenue_share = self.df_summary_customer_revenue_share.append(total, ignore_index=True)

    def get_taxes(self):
        fields = ('dsid', 'site_name') + self.fields_taxes
        data_tmp = self.df_account[list(fields)].drop_duplicates()
        self.df_taxes = data_tmp.set_index("dsid", drop = True)

    def get_package_taxes(self):

        list_package_federal_tax = []
        list_package_provincial_tax = []
        for index, row in self.df_package_tax.iterrows():
            # if math.isnan(row['hst']):
            if row['hst'] is None:
                list_package_federal_tax.append(row['gst'])
                list_package_provincial_tax.append(row['pst'])
            else:
                list_package_federal_tax.append(row['hst'])
                list_package_provincial_tax.append(0.00)
        self.df_package_tax['package_federal_tax'] = list_package_federal_tax
        self.df_package_tax['package_provincial_tax'] = list_package_provincial_tax

        print self.df_package_tax.to_string()


if __name__ == "__main__":

    sql_customer = """
          select distinct customer_name, customer_code
          from meta.site_details
          order by customer_name, customer_code
        """

    customers = get_data_dict_from_hadoop(sql_customer)

    for customer in customers:
        customer_code = customer.get('customer_code')
        customer_name = customer.get('customer_name')

        FIXED_FEES, \
        REVENUE_SHARING_RATIO, \
        PACKAGE_FEDERAL_TAX, \
        PACKAGE_PROVINCIAL_TAX, \
        FEES_FEDERAL_TAX, \
        FEES_PROVINCIAL_TAX = get_default_taxs_fees_by_customer(customer_code)

        # print customer.get('customer_code'), '***************',customer.get('customer_name')
        if customer_code in ('OMH' , 'MDH', 'TMH', 'AHI', 'MKH', 'BCS', 'GBH', 'CHQ','BCH','EGC', 'SLR'):
            rec = ReconciliationReports(customer_code=customer_code,  customer_name=customer_name)
            # rec = ReconciliationReports(first_date='2018-10-01', last_date='2018-10-02', customer_code=customer.get('customer_code'),  customer_name = customer.get('customer_name'))
            rec.get_df_data()
            rec.create_files_xlsx_by_customer_site()


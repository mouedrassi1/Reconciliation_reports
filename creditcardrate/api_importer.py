import urllib2
import json
import os
import codecs
import datetime
import logging
import sys
import subprocess

# to-do:
# 1. move the following to a config file
#    - passed in as an argument

# app config settings
import_name = 'creditcardrate'

hdfs_enabled = True
hive_enabled = True
impala_enabled = True

# log config settings
log = logging.getLogger()
log_to_file = True
log_file_path = '/home/cronuser/crons/hydra/reconciliation_reports/creditcardrate/creditcardrate.log'

# hdfs config settings
hdfs_raw_path = '/user/data/warehouse/raw.db/creditcardrate_raw/creditcardrate.json'
hdfs_archive_path = '/user/data/warehouse/archive.db/raw.db/creditcardrate_raw'

# hive/impala config settings
hive_raw_tablename = 'raw.creditcardrate_raw'
hive_meta_tablename = 'meta.creditcardrate'
hive_update_script = '/home/cronuser/crons/hydra/reconciliation_reports/creditcardrate/hive/update_meta_creditcardrate.hql'

# api config settings
#api_url = 'http://172.27.2.239/site/site?limit={0}&skip={1}'
api_url = 'http://newtown-api-1.prod.ntl.dv/site/creditcardrate?limit={0}&skip={1}'

api_limit = 500


# json config settings
json_filename = '{}_%Y%m%d.json'.format(import_name)
json_directory = '/home/cronuser/crons/hydra/reconciliation_reports/{}/json'.format(import_name)
# json_directory = '/home/mouedrassi/folder/{}/json'.format(import_name)
json_filename_frmt = os.path.join(json_directory, json_filename)

def initLogger():
    log.setLevel(logging.INFO)

    formatter = logging.Formatter(
        '%(asctime)s.%(msecs)d %(levelname)s: %(message)s',
        datefmt='%Y-%m-%d %H:%M:%S')

    consoleHandler = logging.StreamHandler(sys.stdout)
    consoleHandler.setFormatter(formatter)
    log.addHandler(consoleHandler)

    if log_to_file:
        fileHandler = logging.FileHandler(
            filename=log_file_path)
        fileHandler.setFormatter(formatter)
        log.addHandler(fileHandler)

def main():
    initLogger()

    log.info('---------------------------------------------------')
    log.info('import start: ' + import_name)

    if not os.path.exists(json_directory):
        log.info('creating json diretory')
        os.makedirs(json_directory)

    filename = datetime.datetime.now().strftime(json_filename_frmt)
    log.info('filename: %s', filename)

    if os.path.exists(filename):
        log.info('deleting existing file (%s)', filename)
        os.remove(filename)

    api_skip = 0
    api_json_count = 0

    try:
        log.info('api call start')
        url = api_url.format(api_limit, api_skip)
        log.info(url)

        response = urllib2.urlopen(url)
        api_skip += api_limit
        log.info('api call end')

        log.info('URL      : %s', response.geturl())
        log.info('HTTP Code: %s', response.getcode())

        headers = response.info()
        for (key, val) in headers.items():
            log.info('HEADERS  : %s=%r', key, val)

        data = response.read().decode('utf-8')
        api_json = json.loads(data)

        # for the test if the site-api down
        # api_json = [{u'creditcardtype_id': 1, u'payment_process_id': 1, u'credit_card_type_name': u'American Express', u'rate': 0.041, u'payment_process_name': u'Moneris', u'id': 1},
        #             {u'creditcardtype_id': 2, u'payment_process_id': 1, u'credit_card_type_name': u'MasterCard', u'rate': 0.0239, u'payment_process_name': u'Moneris', u'id': 2},
        #             {u'creditcardtype_id': 3, u'payment_process_id': 1, u'credit_card_type_name': u'Visa', u'rate': 0.0239, u'payment_process_name': u'Moneris', u'id': 3}]

        log.info('start - write to json file (total records: %s)', api_json_count)
        with codecs.open(filename, 'a+b', 'utf-8-sig') as outfile:
            for json_record in api_json:
                if api_json_count != 0:
                    outfile.write('\n')
                outfile.write(json.dumps(json_record, ensure_ascii=False))
                api_json_count += 1
        log.info('end - write to json file (total records: %s)', api_json_count)

    except urllib2.HTTPError as err:
        if err.code == 400:
            log.info('reached end of api json (http return code: 400)')
        else:
            log.error('unexpected error code: %s', err.code)

    if hdfs_enabled:
        log.info('start - hdfs storage')

        log.info('copy json to hdfs raw')
        hdfs_raw_args = ['hdfs', 'dfs', '-copyFromLocal', '-f', filename, hdfs_raw_path]
        log.info(' '.join(hdfs_raw_args))
        subprocess.check_call(hdfs_raw_args)

        log.info('copy json to hdfs archive')
        hdfs_archive_args = ['hdfs', 'dfs', '-copyFromLocal', '-f', filename, hdfs_archive_path]
        log.info(' '.join(hdfs_archive_args))
        subprocess.check_call(hdfs_archive_args)

        log.info('end - hdfs storage')

    if hive_enabled:
        log.info('start - hive processing')

        log.info('refresh raw table')
        hive_msck_args = ['hive', '-e', 'msck repair table ' + hive_raw_tablename + ';']
        log.info(' '.join(hive_msck_args))
        subprocess.check_call(hive_msck_args)

        log.info('update meta table')
        hive_update_args = ['hive', '-f', hive_update_script]
        log.info(' '.join(hive_update_args))
        subprocess.check_call(hive_update_args)

        log.info('end - hive processing')

    if impala_enabled:
        log.info('start - impala processing')

        log.info('refresh meta table')
        impala_refresh_args = ['impala-shell', '-q', 'refresh ' + hive_meta_tablename + ';']
        log.info(' '.join(impala_refresh_args))
        subprocess.check_call(impala_refresh_args)

        log.info('compute stats')
        impala_stats_args = ['impala-shell', '-q', 'compute stats ' + hive_meta_tablename + ';']
        log.info(' '.join(impala_stats_args))
        subprocess.check_call(impala_stats_args)

        log.info('end - impala processing')

    log.info('import end: ' + import_name)


if __name__ == '__main__':
    main()

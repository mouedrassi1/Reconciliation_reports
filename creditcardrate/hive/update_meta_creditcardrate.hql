msck repair table raw.creditcardrate_raw;
insert overwrite table meta.creditcardrate
select *,
FROM_UNIXTIME(UNIX_TIMESTAMP(),'yyyy-MM-dd') as date
from raw.creditcardrate_raw;
msck repair table meta.creditcardrate;

#!/bin/bash
#!/usr/bin/env python

LOG="/home/cronuser/crons/hydra/reconciliation_reports/reconciliation_reports.log"

echo -e "-----------------------------------------------------------------" >> ${LOG}
echo "start: " $(date) >> ${LOG}
echo "" >> ${LOG}

# generate csv files
python /home/cronuser/crons/hydra/reconciliation_reports/report_df_total.py >> ${LOG}


# copy_csv_to_reports_revenue_share
python /home/cronuser/crons/hydra/reconciliation_reports/copy_csv_to_reports_revenue_share.py >> ${LOG}

echo -e "\nend: " $(date) >> ${LOG}
echo -e "-----------------------------------------------------------------" >> ${LOG}



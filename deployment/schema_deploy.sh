#!/bin/bash
#!/usr/bin/env python

LOG="/home/cronuser/crons/hydra/logs/update_meta_site_details.log"

echo -e "-----------------------------------------------------------------" >> ${LOG}
echo "start: " $(date) >> ${LOG}
echo "" >> ${LOG}

BEELINE_IP="172.20.22.10:10000"
IMPALA_IP="172.20.22.10:25003"

#BEELINE_IP="172.30.136.55:10000"
#IMPALA_IP="172.30.136.55:25003"

#meta_site_details
beeline -u jdbc:hive2://$BEELINE_IP -n cronuser -f /home/cronuser/crons/hydra/reconciliation_reports/deployment/hive/update_meta_site_details.sql >> ${LOG}
impala-shell -i $IMPALA_IP -f /home/cronuser/crons/hydra/reconciliation_reports/deployment/impala/update_meta_site_details.sql >> ${LOG}

#derived_account_details_report
beeline -u jdbc:hive2://$BEELINE_IP -n cronuser -f /home/cronuser/crons/hydra/reconciliation_reports/deployment/hive/update_derived.account_details_report.sql >> ${LOG}
impala-shell -i $IMPALA_IP -f /home/cronuser/crons/hydra/reconciliation_reports/deployment/impala/account_details_report_impala.sql >> ${LOG}


echo -e "\nend: " $(date) >> ${LOG}
echo -e "-----------------------------------------------------------------" >> ${LOG}








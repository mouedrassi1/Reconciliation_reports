#!/bin/bash
#!/usr/bin/env python

LOG="/home/cronuser/crons/hydra/reconciliation_reports/reconciliation_reports.log"

echo -e "-----------------------------------------------------------------" >> ${LOG}
echo "start: " $(date) >> ${LOG}
echo "" >> ${LOG}

BEELINE_IP="172.20.22.10:10000"
IMPALA_IP="172.20.22.10:25003"

#BEELINE_IP="172.30.136.55:10000"
#IMPALA_IP="172.30.136.55:25003"

# creditcardrate
python /home/cronuser/crons/hydra/reconciliation_reports/creditcardrate/api_importer.py >> ${LOG}

# site_taxes
beeline -u jdbc:hive2://$BEELINE_IP -n cronuser  -f /home/cronuser/crons/hydra/reconciliation_reports/site_taxes/hive/update_meta_site_taxes.hql >> ${LOG}
impala-shell -i $IMPALA_IP -f /home/cronuser/crons/hydra/reconciliation_reports/site_taxes/impala/site_taxes_impala.sql >> ${LOG}

# account_details_report
python /home/cronuser/crons/hydra/reconciliation_reports/account_details_report/hive/update_derived_account_details_report.py >> ${LOG}
impala-shell -i $IMPALA_IP -f /home/cronuser/crons/hydra/reconciliation_reports/account_details_report/impala/account_details_report_impala.sql >> ${LOG}


echo -e "\nend: " $(date) >> ${LOG}
echo -e "-----------------------------------------------------------------" >> ${LOG}





tables requirements

1: /user/data/warehouse/raw.db/account_details_raw   (avro file)
2: /user/data/warehouse/derived.db/account_details_report
3: /user/data/warehouse/meta.db/site_details  ( fixed_fees,
                                                revenue_sharing_ratio,
                                                package_federal_tax,
                                                package_provincial_tax,
                                                fees_federal_tax,
                                                fees_provincial_tax )
4: /user/data/warehouse/meta.db/site_taxes
5: /user/data/warehouse/meta.db/creditcardrate


=> SELECT id, name FROM portal.authtype; (21;"Paypal", 22;"PCICreditCard")

=> generate derived.db/account_details_report from raw.db/account_details_raw between two dates and filtered by pkg_authtype_id
=> generate meta.db/site_details from raw.site_details_raw
=> generate meta.db/site_taxes from raw.site_details_raw
=> generate meta.db/creditcardrate from raw.creditcardrate_raw
=> generate raw.creditcardrate_raw form (api_importer.py => api_url = 'http://newtown-api-1.prod.ntl.dv/site/creditcardrate?limit={0}&skip={1}')

##############################
add file hdfs:/user/data/python/extract_attributes1.py;

set hive.exec.dynamic.partition = true;
set hive.exec.dynamic.partition.mode = nonstrict;
msck repair table raw.account_details_raw;

insert overwrite table default.account_details_report  partition(year, month, day)

select
TRANSFORM(
account_details.record_uuid ,
account_details.site_uuid ,
account_details.pkg_customerid,
account_details.device_mac,
regexp_replace(account_details.pkg_name, '\\\\t|\\\\n', ' '),
from_unixtime(account_details.account_startdate),
account_details.year,
account_details.month,
account_details.day,
regexp_replace(account_details.account_attributes, '\\\\t|\\\\n', ' '))
USING 'python extract_attributes1.py' AS (record_uuid, site_uuid, pkg_customerid,device_mac,pkg_name, account_startdate,
                 access_code_token, name_on_card, purchase_amount, transaction_number,
                 order_id, access_code_uses, email, card_type, payment_processor,
                 base_price, gst, pst, hst, donationamount,
                 year,month,day)
from
(select
record_uuid,
site_uuid,
pkg_customerid,
device_mac,
pkg_name,
account_startdate,
year,
month,
day,
account_attributes from raw.account_details_raw
where
to_date(concat(cast(year as string),'-', LPAD(cast(month as string),2,'0'),'-', LPAD(cast(day as string),2,'0')))
between
'2018-12-01' and
'2018-12-31' and
pkg_authtype_id in (21, 22)) account_details;


msck repair table default.account_details_report;
analyze table default.account_details_report partition(year, month, day) compute statistics;
###############################

Note: to do the test on backup use this file =>
cronuser@hydra-dr-3:~/crons/hydra/med$
/home/cronuser/crons/hydra/med/update_derived_account_detail_report.sql

beeline -u jdbc:hive2://172.30.136.57:10000 -n cronuser -f /home/cronuser/crons/hydra/med/update_derived_account_detail_report.sql


Note: to do the test on prod use this file =>
cronuser@hydra-jobtracker-1:~/crons/hydra/reconciliation_reports/deployment$
/home/cronuser/crons/hydra/reconciliation_reports/deployment/test_update_default_account_details.sql

beeline -u jdbc:hive2://172.20.22.10:10000 -n cronuser -f /home/cronuser/crons/hydra/reconciliation_reports/deployment/test_update_default_account_details.sql



taxes
beeline -u jdbc:hive2://172.20.22.10:10000 -n cronuser -f /home/cronuser/crons/hydra/reconciliation_reports/deployment/test_update_default_account_details.sql
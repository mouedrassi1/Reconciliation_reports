#!/bin/bash
#!/usr/bin/env python

LOG="/home/cronuser/crons/hydra/reconciliation_reports/reconciliation_reports.log"

echo -e "-----------------------------------------------------------------" >> ${LOG}
echo "start: " $(date) >> ${LOG}
echo "" >> ${LOG}

# creditcardrate
python /home/cronuser/crons/hydra/reconciliation_reports/creditcardrate/api_importer.py >> ${LOG}

# customer_details
hive -f /home/cronuser/crons/hydra/reconciliation_reports/customer_details/hive/update_meta_customer_details.hql >> ${LOG}
impala-shell -f /home/cronuser/crons/hydra/reconciliation_reports/customer_details/impala/customer_details_impala.sql >> ${LOG}

# account_details_report
python /home/cronuser/crons/hydra/reconciliation_reports/account_details_report/hive/update_derived_account_details_report.py >> ${LOG}
impala-shell -f /home/cronuser/crons/hydra/reconciliation_reports/account_details_report/impala/account_details_report_impala.sql >> ${LOG}


echo -e "\nend: " $(date) >> ${LOG}
echo -e "-----------------------------------------------------------------" >> ${LOG}




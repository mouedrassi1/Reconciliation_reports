import sys
import ast

'''
"1";"AccessCodeToken"
"2";"NameOnCard"
"3";"PurchaseAmount"
"4";"TransactionNumber"
"5";"OrderId"
"9";"AccessCodeUses"
"10";"Miscellaneous"
"11";"Email"
"12";"CardType"
"13";"PaymentProcessor"
"14";"BasePrice"
"15";"GST"
"16";"PST"
"17";"HST"
'''
for line in sys.stdin:
    line = line.strip()
    record_uuid, site_uuid, pkg_customerid ,device_mac,pkg_name, account_startdate, year, month, day, attributes = line.split('\t')
    data = ast.literal_eval(attributes)
    attribute_values = {}
    attributeTypeIds = [1, 2, 3, 4, 5, 9, 11, 12, 13, 14, 15, 16, 17]
    for i in attributeTypeIds:
        attr_val = ''
        list_attr_val = [d for d in data if d.get('attributeTypeId', 0) == i]
        if list_attr_val:
            attr_val = list_attr_val[0].get('attributeValue', 0)
        attribute_values[i] = attr_val

    list_attribute_values = []
    total_items = []
    for i in attributeTypeIds:
    	list_attribute_values.append(attribute_values[i])

    total_items = [record_uuid, site_uuid, pkg_customerid,device_mac,pkg_name, account_startdate]
    total_items.extend(list_attribute_values)
    total_items.extend( [year,month,day])

    print '\t'.join(total_items)

add file /user/data/python/extract_attributes.py;

set hive.exec.dynamic.partition = true;
set hive.exec.dynamic.partition.mode = nonstrict;
msck repair table raw.account_details_raw;

insert overwrite table derived.account_details_report  partition(year, month, day)

select
TRANSFORM(
account_details.record_uuid ,
account_details.site_uuid ,
account_details.pkg_customerid,
account_details.device_mac,
regexp_replace(account_details.pkg_name, '\\\\t|\\\\n', ' '),
from_unixtime(account_details.account_startdate),
account_details.year,
account_details.month,
account_details.day,
regexp_replace(account_details.account_attributes, '\\\\t|\\\\n', ' '))
USING 'python extract_attributes.py' AS (record_uuid, site_uuid, pkg_customerid,device_mac,pkg_name, account_startdate,
                 access_code_token, name_on_card, purchase_amount, transaction_number,
                 order_id, access_code_uses, email, card_type, payment_processor,
                 base_price, gst, pst, hst,
                 year,month,day)
from
(select
record_uuid,
site_uuid,
pkg_customerid,
device_mac,
pkg_name,
account_startdate,
year,
month,
day,
account_attributes from raw.account_details_raw
where
to_date(concat(cast(year as string),'-', LPAD(cast(month as string),2,'0'),'-', LPAD(cast(day as string),2,'0')))
between
'2017-05-17' and
'2017-05-18' and
pkg_authtype_id = 22) account_details;


msck repair table derived.account_details_report;
analyze table derived.account_details_report partition(year, month, day) compute statistics;




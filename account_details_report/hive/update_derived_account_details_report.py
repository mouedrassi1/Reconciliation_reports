#!/usr/bin/env python
import datetime
import logging
import sys
import pyhs2

HADOOP_HOST="172.30.136.17"
HADOOP_PORT=10000
USER='root'
DATABASE='default'

# log config settings
log = logging.getLogger()
log_to_file = False
log_file_path = '/home/cronuser/crons/hydra/reconciliation_reports/account_details_report/account_details_report.log'

def initLogger():
    log.setLevel(logging.INFO)

    formatter = logging.Formatter(
        '%(asctime)s.%(msecs)d %(levelname)s: %(message)s',
        datefmt='%Y-%m-%d %H:%M:%S')

    consoleHandler = logging.StreamHandler(sys.stdout)
    consoleHandler.setFormatter(formatter)
    log.addHandler(consoleHandler)

    if log_to_file:
        fileHandler = logging.FileHandler(
            filename=log_file_path)
        fileHandler.setFormatter(formatter)
        log.addHandler(fileHandler)


class UpdateAccountDetails(object):

    def __init__(self, date_in=''):

        if date_in:
            first_date = date_in - datetime.timedelta(days=1)
            first_date = first_date.replace(hour=0, minute=0)
            last_date = date_in
        else:
            first_date = datetime.datetime.now() - datetime.timedelta(days=1)
            first_date = first_date.replace(hour=0, minute=0)
            last_date = datetime.datetime.now()

        sql = """
        add file hdfs://hydra-1-admin.c6.dv:8020/user/data/python/extract_attributes.py;

        set hive.exec.dynamic.partition = true;
        set hive.exec.dynamic.partition.mode = nonstrict;
        msck repair table raw.account_details_raw;

        insert overwrite table derived.account_details_report  partition(year, month, day)

        select
        TRANSFORM(
        account_details.record_uuid ,
        account_details.site_uuid ,
        account_details.pkg_customerid,
        account_details.device_mac,
        regexp_replace(account_details.pkg_name, '{3}', ' '),
        from_unixtime(account_details.account_startdate),
        account_details.year,
        account_details.month,
        account_details.day,
        regexp_replace(account_details.account_attributes, '{3}', ' '))
        USING 'python extract_attributes.py' AS (record_uuid, site_uuid, pkg_customerid,device_mac,pkg_name, account_startdate,
                         access_code_token, name_on_card, purchase_amount, transaction_number,
                         order_id, access_code_uses, email, card_type, payment_processor,
                         base_price, gst, pst, hst,
                         year,month,day)
        from
        (select
        record_uuid,
        site_uuid,
        pkg_customerid,
        device_mac,
        pkg_name,
        account_startdate,
        year,
        month,
        day,
        account_attributes from raw.account_details_raw
        where
        to_date(concat(cast(year as string),'-', LPAD(cast(month as string),2,'0'),'-', LPAD(cast(day as string),2,'0')))
        between
        '{0}' and
        '{1}' and
        pkg_authtype_id = {2}) account_details;


        msck repair table derived.account_details_report;
        analyze table derived.account_details_report partition(year, month, day) compute statistics;
        """.format(first_date.date(), last_date.date(), 22, "\\\\\\\\t|\\\\\\\\n")

        self.list_sql = sql.split(';')
        self.list_sql = self.list_sql[:-1]
        print self.list_sql

    def update_account_details(self):

        with pyhs2.connect(host=HADOOP_HOST,
                               port=HADOOP_PORT,
                               authMechanism="PLAIN",
                               user=USER,
                               password='',
                               database=DATABASE) as conn:
            with conn.cursor() as cur:
                #Show databases
                try:
                    print cur.getDatabases()
                    for sql in self.list_sql:
                        sql = sql.replace('\n', ' ').strip()
                        cur.execute(sql)
                        print sql, ';'

                except Exception as e:
                    # log.exception(e)
                    log.error(e)
                    print e
                except RuntimeError as re:
                    # log.exception(re)
                    log.error(re)
                    print re
                finally:
                    conn.close()

if __name__ == "__main__":

    account_details = UpdateAccountDetails()
    account_details.update_account_details()

import subprocess

user = 'reportsvc'
password = 'oashaiC#a5nu'
destination = 'finance/reports_revenue_share'
source = '/home/cronuser/crons/hydra/reconciliation_reports/data/'

# cmd = """
# smbclient -d3 //ubilium.loc/fs/ -U {0}%{1} -W UBILIUM --directory {2} -c 'lcd "{3}";prompt;recurse ON; mput "*"'
# """.format(user, password, destination, source)

cmd = """
smbclient //ubilium.loc/fs/ -U {user}%{password} -W UBILIUM --directory {destination} -c 'lcd "{source}";prompt;recurse ON; mput "*"'
""".format(user=user, password=password, destination=destination, source=source)

if __name__ == "__main__":
    subprocess.check_call(cmd, shell=True)
    # print cmd
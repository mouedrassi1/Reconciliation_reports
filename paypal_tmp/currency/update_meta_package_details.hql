-- The old sql

--cat update_meta_package_details.hql
msck repair table raw.package_details_raw;

insert overwrite table meta.package_details
select
id
, uuid
, name
, cast(authtype.id as bigint) as authtype_id
, authtype.name as authtype_name
from raw.package_details_raw;

msck repair table meta.package_details;
ANALYZE TABLE meta.package_details COMPUTE STATISTICS;


--the new sql

msck repair table raw.package_details_raw;

insert overwrite table meta.package_details

select
pdr.id,
pdr.uuid,
pdr.name,
cast(pdr.authtype.id as bigint) as authtype_id,
pdr.authtype.name as authtype_name,
cur.currency
from raw.package_details_raw pdr
left outer join (
	select uuid, exploded_config.settingvalue as currency
	from raw.package_details_raw
	lateral view explode(config) exploded_table as exploded_config
where exploded_config.settingtypeid = 22
) cur on pdr.uuid = cur.uuid;

msck repair table meta.package_details;
ANALYZE TABLE meta.package_details COMPUTE STATISTICS;








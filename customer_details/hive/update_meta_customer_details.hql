set hive.exec.dynamic.partition = true;
set hive.exec.dynamic.partition.mode = nonstrict;
msck repair table raw.site_details_raw;

insert overwrite table meta.customer_details  partition(year, month)

select
customer.customer_id as customer_id,
customer.customer_name as customer_name,
customer.customer_code as customer_code,
MAX(CASE WHEN attr.typename = 'FixedFees' THEN attr.value ELSE ''END) AS fixed_fees,
MAX(CASE WHEN attr.typename = 'RevenueSharing_Ratio' THEN attr.value ELSE ''END) AS revenue_sharing_ratio,

day(to_utc_timestamp(from_unixtime(UNIX_TIMESTAMP()), 'Canada/Eastern')) as day,
year(to_utc_timestamp(from_unixtime(UNIX_TIMESTAMP()), 'Canada/Eastern')) as year,
month(to_utc_timestamp(from_unixtime(UNIX_TIMESTAMP()), 'Canada/Eastern')) as month

from (select distinct
       sdr.customer.id as customer_id,
       sdr.customer.name as customer_name,
       sdr.customer.code as customer_code
       from raw.site_details_raw sdr)
        customer
 	      LEFT JOIN

	       (select distinct
	        sdr.customer.id customer_id,
	        cust_attr.typename typename,
	        cust_attr.value value
	        from raw.site_details_raw sdr
	        lateral view explode(customer.attributes) et as cust_attr)
              attr on customer.customer_id = attr.customer_id
GROUP BY customer.customer_id, customer.customer_code, customer.customer_name;

msck repair table meta.customer_details;
analyze table meta.customer_details partition(year, month) compute statistics;





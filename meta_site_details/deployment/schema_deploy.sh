#!/bin/bash
#!/usr/bin/env python

LOG="/home/cronuser/crons/hydra/logs/update_meta_site_details.log"

echo -e "-----------------------------------------------------------------" >> ${LOG}
echo "start: " $(date) >> ${LOG}
echo "" >> ${LOG}

BEELINE_IP="172.20.22.10:10000"
IMPALA_IP="172.20.22.10:25003"

#BEELINE_IP="172.30.136.55:10000"
#IMPALA_IP="172.30.136.55:25003"

beeline -u jdbc:hive2://$BEELINE_IP -n cronuser -f /home/cronuser/crons/hydra/meta/site_details/deployment/hive/update_meta_site_details.sql >> ${LOG}
impala-shell -i $IMPALA_IP -f /home/cronuser/crons/hydra/meta/site_details/deployment/impala/update_meta_site_details.sql >> ${LOG}


echo -e "\nend: " $(date) >> ${LOG}
echo -e "-----------------------------------------------------------------" >> ${LOG}




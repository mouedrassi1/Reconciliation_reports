import urllib2
import json
import os
import codecs
import datetime
import logging
import sys
import subprocess

# to-do:
# 1. move the following to a config file
#    - passed in as an argument

# app config settings
import_name = 'site_details'

BEELINE_IP='172.20.22.10:10000'
IMPALA_IP='172.20.22.10:25003'
HDFS_IP='172.20.22.2:8020'
USERNAME='cronuser'

hdfs_enabled = True
hive_enabled = True
impala_enabled = True

# log config settings
log = logging.getLogger()
log_to_file = False
log_file_path = '/home/cronuser/crons/hydra/meta/site_details/site_details.log'

# hdfs config settings
hdfs_raw_path = 'hdfs://{}/user/data/warehouse/raw.db/site_details_raw/site_details.json'.format(HDFS_IP)
hdfs_archive_path = 'hdfs://{}/user/data/warehouse/archive.db/raw.db/site_details_raw'.format(HDFS_IP)

# hive/impala config settings
hive_raw_tablename = 'raw.site_details_raw'
hive_meta_tablename = 'meta.site_details'
hive_update_script = '/home/cronuser/crons/hydra/meta/site_details/hive/update_meta_site_details.hql'

# api config settings
#api_url = 'http://172.27.2.239/site/site?limit={0}&skip={1}'
#api_url = 'http://sfapi-1.c6.dv/site/site?limit={0}&skip={1}'
api_url = 'http://apis.arcadia.prod.dv/site/site?limit={0}&skip={1}'
#api_url = 'http://newtown-api-1.prod.ntl.dv/site/site?limit={0}&skip={1}'
api_limit = 50

# json config settings
json_filename_frmt = '/home/cronuser/crons/hydra/meta/site_details/json/site_details_%Y%m%d.json'

def initLogger():
    log.setLevel(logging.INFO)

    formatter = logging.Formatter(
        '%(asctime)s.%(msecs)d %(levelname)s: %(message)s',
        datefmt='%Y-%m-%d %H:%M:%S')

    consoleHandler = logging.StreamHandler(sys.stdout)
    consoleHandler.setFormatter(formatter)
    log.addHandler(consoleHandler)

    if log_to_file:
        fileHandler = logging.FileHandler(
            filename=log_file_path)
        fileHandler.setFormatter(formatter)
        log.addHandler(fileHandler)

def main():
    initLogger()

    log.info('---------------------------------------------------')
    log.info('import start: ' + import_name)

    log.info('BEELINE_IP: %s ', BEELINE_IP)
    log.info('IMPALA_IP: %s ', IMPALA_IP)
    log.info('HDFS_IP: %s ', HDFS_IP)
    log.info('USERNAME: %s ', USERNAME)

    filename = datetime.datetime.now().strftime(json_filename_frmt)
    log.info('filename: %s', filename)

    if os.path.exists(filename):
        log.info('deleting existing file (%s)', filename)
        os.remove(filename)

    api_skip = 0
    api_json_count = 0

    while True:
        try:
            log.info('api call start')
            url = api_url.format(api_limit, api_skip)
            log.info(url)

            response = urllib2.urlopen(url, timeout=120)
            api_skip += api_limit
            log.info('api call end')

            log.info('URL      : %s', response.geturl())
            log.info('HTTP Code: %s', response.getcode())

            headers = response.info()
            for (key, val) in headers.items():
                log.info('HEADERS  : %s=%r', key, val)

            data = response.read().decode('utf-8')
            api_json = json.loads(data)
            if len(api_json) == 0:
                log.info('reached end of api json (json record count == 0)')
                break

            log.info('start - write to json file (total records: %s)', api_json_count)
            with codecs.open(filename, 'a+b', 'utf-8-sig') as outfile:
                for json_record in api_json:
                    if api_json_count != 0:
                        outfile.write('\n')
                    outfile.write(json.dumps(json_record, ensure_ascii=False))
                    api_json_count += 1
            log.info('end - write to json file (total records: %s)', api_json_count)

        except urllib2.HTTPError as err:
            if err.code == 400:
                log.info('reached end of api json (http return code: 400)')
            else:
                log.error('unexpected error code: %s', err.code)
            break

    if hdfs_enabled:
        log.info('start - hdfs storage')

        log.info('copy json to hdfs raw')
        hdfs_raw_args = ['hdfs', 'dfs', '-copyFromLocal', '-f', filename, hdfs_raw_path]
        log.info(' '.join(hdfs_raw_args))
        subprocess.check_call(hdfs_raw_args)

        log.info('copy json to hdfs archive')
        hdfs_archive_args = ['hdfs', 'dfs', '-copyFromLocal', '-f', filename, hdfs_archive_path]
        log.info(' '.join(hdfs_archive_args))
        subprocess.check_call(hdfs_archive_args)

        log.info('end - hdfs storage')

    if hive_enabled:
        log.info('start - hive processing')

        #beeline -u jdbc:hive2://172.30.136.57:10000 -n cronuser -e 'msck repair table raw.site_details_raw';

        log.info('refresh raw table')
        hive_msck_args = ['beeline', '-u', 'jdbc:hive2://' + BEELINE_IP, '-n', USERNAME, '-e', 'msck repair table ' + hive_raw_tablename + ';']
        log.info(' '.join(hive_msck_args))
        subprocess.check_call(hive_msck_args)

        #beeline -u jdbc:hive2://$BEELINE_IP -n cronuser -f /home/cronuser/crons/hydra/meta/hardware_details/hive/hardware_details_hive.sql

        log.info('update meta table')
        hive_update_args = ['beeline', '-u', 'jdbc:hive2://' + BEELINE_IP, '-n', USERNAME, '-f', hive_update_script]
        log.info(' '.join(hive_update_args))
        subprocess.check_call(hive_update_args)

        log.info('end - hive processing')

    if impala_enabled:
        log.info('start - impala processing')

        #impala-shell -i 172.30.136.55:25003 -u cronuser -q 'refresh meta.site_details;'

        log.info('refresh meta table')
        impala_refresh_args = ['impala-shell', '-i', IMPALA_IP, '-u', USERNAME, '-q', 'refresh ' + hive_meta_tablename + ';']
        log.info(' '.join(impala_refresh_args))
        subprocess.check_call(impala_refresh_args)

        log.info('compute stats')
        impala_stats_args = ['impala-shell', '-i', IMPALA_IP, '-u', USERNAME, '-q', 'compute stats ' + hive_meta_tablename + ';']
        log.info(' '.join(impala_stats_args))
        subprocess.check_call(impala_stats_args)

        log.info('end - impala processing')

    log.info('import end: ' + import_name)


if __name__ == '__main__':
    main()

 --update /home/cronuser/crons/hydra/meta/site_details/hive/update_meta_site_details.hql, by =>

msck repair table raw.site_details_raw;

set hive.exec.dynamic.partition = true;
set hive.exec.dynamic.partition.mode = nonstrict;
set hive.exec.max.dynamic.partitions = 5000;
set hive.exec.max.dynamic.partitions.pernode = 5000;
set hive.exec.reducers.bytes.per.reducer= 500;
set hive.exec.reducers.max= 100;

set mapreduce.map.memory.mb = 2024;
set mapreduce.reduce.memory.mb = 2024;
set mapreduce.map.java.opts = -Xmx1228m;
set mapreduce.reduce.java.opts = -Xmx1228m;

insert overwrite table meta.site_details

select distinct
	sd1.uuid as site_uuid,
	sd2.customer_sitename as site_name,
	sd2.customer_sitename as customer_site_name,
    sd1.name as datavalet_site_name,
	sd3.transit_number as transit_number,
	sd1.brand.name as brand_name,
	sd1.customer.name as customer_name,
	sd1.customer.code as customer_code,
	sd1.status.name as status,
	sd1.marketsegment.name as market_segment,
	sd1.address.country.name as country,
	sd1.address.province.name as province,
	sd1.address.city.name as city,
	sd1.address.street as street,
	sd1.address.postalcode as postal_code,
	sd1.address.latitude as latitude,
	sd1.address.longitude as longitude,
	sd1.address.timezone as timezone,
	sd1.name as dsid,
	sd4.bsi_id as bsi_id,

	if(isnull(sd5.fixed_fees), sd11.fixed_fees_cus, sd5.fixed_fees) as fixed_fees,
	if(isnull(sd6.revenue_sharing_ratio), sd12.revenue_sharing_ratio_cus, sd6.revenue_sharing_ratio) as revenue_sharing_ratio,
	if(isnull(sd7.package_federal_tax), sd13.package_federal_tax_cus, sd7.package_federal_tax) as package_federal_tax,
	if(isnull(sd8.package_provincial_tax), sd14.package_provincial_tax_cus, sd8.package_provincial_tax) as package_provincial_tax,
	if(isnull(sd9.fees_federal_tax), sd15.fees_federal_tax_cus, sd9.fees_federal_tax) as fees_federal_tax,
	if(isnull(sd10.fees_provincial_tax), sd16.fees_provincial_tax_cus, sd10.fees_provincial_tax) as fees_provincial_tax

from raw.site_details_raw sd1
left outer join (
	select uuid, exploded_attrs.value as customer_sitename
	from raw.site_details_raw
	lateral view explode(attributes) exploded_table as exploded_attrs
	where exploded_attrs.typeid = 8
) sd2 on sd1.uuid = sd2.uuid
left outer join (
	select uuid, exploded_attrs.value as transit_number
	from raw.site_details_raw
	lateral view explode(attributes) exploded_table as exploded_attrs
	where exploded_attrs.typeid = 9
) sd3 on sd1.uuid = sd3.uuid
left outer join (
	select uuid, exploded_attrs.value as bsi_id
	from raw.site_details_raw
	lateral view explode(attributes) exploded_table as exploded_attrs
	where exploded_attrs.typeid = 2
) sd4 on sd1.uuid = sd4.uuid

--------------------site Attributes--------------------------

left outer join (
	select uuid, exploded_attrs.value as fixed_fees
	from raw.site_details_raw
	lateral view explode(attributes) exploded_table as exploded_attrs
	where exploded_attrs.typeid = 26
) sd5 on sd1.uuid = sd5.uuid

left outer join (
	select uuid, exploded_attrs.value as revenue_sharing_ratio
	from raw.site_details_raw
	lateral view explode(attributes) exploded_table as exploded_attrs
	where exploded_attrs.typeid = 27
) sd6 on sd1.uuid = sd6.uuid

left outer join (
	select uuid, exploded_attrs.value as package_federal_tax
	from raw.site_details_raw
	lateral view explode(attributes) exploded_table as exploded_attrs
	where exploded_attrs.typeid = 28
) sd7 on sd1.uuid = sd7.uuid

left outer join (
	select uuid, exploded_attrs.value as package_provincial_tax
	from raw.site_details_raw
	lateral view explode(attributes) exploded_table as exploded_attrs
	where exploded_attrs.typeid = 29
) sd8 on sd1.uuid = sd8.uuid

left outer join (
	select uuid, exploded_attrs.value as fees_federal_tax
	from raw.site_details_raw
	lateral view explode(attributes) exploded_table as exploded_attrs
	where exploded_attrs.typeid = 30
) sd9 on sd1.uuid = sd9.uuid

left outer join (
	select uuid, exploded_attrs.value as fees_provincial_tax
	from raw.site_details_raw
	lateral view explode(attributes) exploded_table as exploded_attrs
	where exploded_attrs.typeid = 31
) sd10 on sd1.uuid = sd10.uuid

--------------------------------customer Attributes----------------------------

left outer join (
	select uuid, exploded_attrs.value as fixed_fees_cus
	from raw.site_details_raw
	lateral view explode(customer.attributes) exploded_table as exploded_attrs
	where exploded_attrs.typeid = 6
) sd11 on sd1.uuid = sd11.uuid

left outer join (
	select uuid, exploded_attrs.value as revenue_sharing_ratio_cus
	from raw.site_details_raw
	lateral view explode(customer.attributes) exploded_table as exploded_attrs
	where exploded_attrs.typeid = 7
) sd12 on sd1.uuid = sd12.uuid

left outer join (
	select uuid, exploded_attrs.value as package_federal_tax_cus
	from raw.site_details_raw
	lateral view explode(customer.attributes) exploded_table as exploded_attrs
	where exploded_attrs.typeid = 10
) sd13 on sd1.uuid = sd13.uuid

left outer join (
	select uuid, exploded_attrs.value as package_provincial_tax_cus
	from raw.site_details_raw
	lateral view explode(customer.attributes) exploded_table as exploded_attrs
	where exploded_attrs.typeid = 11
) sd14 on sd1.uuid = sd14.uuid

left outer join (
	select uuid, exploded_attrs.value as fees_federal_tax_cus
	from raw.site_details_raw
	lateral view explode(customer.attributes) exploded_table as exploded_attrs
	where exploded_attrs.typeid = 12
) sd15 on sd1.uuid = sd15.uuid

left outer join (
	select uuid, exploded_attrs.value as fees_provincial_tax_cus
	from raw.site_details_raw
	lateral view explode(customer.attributes) exploded_table as exploded_attrs
	where exploded_attrs.typeid = 13
) sd16 on sd1.uuid = sd16.uuid;

msck repair table meta.site_details;
analyze table meta.site_details compute statistics;

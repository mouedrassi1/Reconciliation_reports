--cat update_meta_site_details.hql
msck repair table raw.site_details_raw;

insert overwrite table meta.site_details
select distinct
	sd1.uuid as site_uuid,
	sd2.customer_sitename as site_name,
	sd2.customer_sitename as customer_site_name,
    sd1.name as datavalet_site_name,
	sd3.transit_number as transit_number,
	sd1.brand.name as brand_name,
	sd1.customer.name as customer_name,
	sd1.customer.code as customer_code,
	sd1.status.name as status,
	sd1.marketsegment.name as market_segment,
	sd1.address.country.name as country,
	sd1.address.province.name as province,
	sd1.address.city.name as city,
	sd1.address.street as street,
	sd1.address.postalcode as postal_code,
	sd1.address.latitude as latitude,
	sd1.address.longitude as longitude,
	sd1.address.timezone as timezone,
	sd1.name as dsid,
	sd4.bsi_id as bsi_id
from raw.site_details_raw sd1
left outer join (
	select uuid, exploded_attrs.value as customer_sitename
	from raw.site_details_raw
	lateral view explode(attributes) exploded_table as exploded_attrs
	where exploded_attrs.typeid = 8
) sd2 on sd1.uuid = sd2.uuid
left outer join (
	select uuid, exploded_attrs.value as transit_number
	from raw.site_details_raw
	lateral view explode(attributes) exploded_table as exploded_attrs
	where exploded_attrs.typeid = 9
) sd3 on sd1.uuid = sd3.uuid
left outer join (
	select uuid, exploded_attrs.value as bsi_id
	from raw.site_details_raw
	lateral view explode(attributes) exploded_table as exploded_attrs
	where exploded_attrs.typeid = 2
) sd4 on sd1.uuid = sd4.uuid;

msck repair table meta.site_details;

import csv
import datetime
import json
import os
import errno
from impala.dbapi import connect
import calendar
from itertools import groupby, tee
from operator import itemgetter

#HADOOP_HOST="172.30.54.174"
HADOOP_HOST="172.30.136.17"
HADOOP_PORT=21050
HADOOP_LIMIT_MINUTES=20

DEFAULT_CREDIT_CARD_RATE = {'American Express': 4.25, 'MasterCard': 3.25, 'Visa': 3.25}

FIXED_FEES = 0.90
REVENUE_SHARING_RATIO = 50

PACKAGE_FEDERAL_TAX = 0.13
PACKAGE_PROVINCIAL_TAX = 0
FEES_FEDERAL_TAX = 0.05
FEES_PROVINCIAL_TAX = 0


# Package federal tax could be any of these values: 0%, 5%, 13% or 15%.
# Package provincial tax could be any of these values: 0%, 6%, 7%, 8% or 9.975%.
# Fees federal tax could be any of these values: 0%, 5%, 13% or 15%.
# Fees provincial tax could be any of these values: 0% or 9.975%.

BASE_SAVE_FOLDER = '/home/mouedrassi/folder'


def get_data_dict_from_hadoop(sql_in):

        host = HADOOP_HOST
        port = HADOOP_PORT

        data_out = []
        # log.debug('sql execute query "{0}" on server {1} port {2}'.format(sql_in, host, port))
        try:
            conn = connect(host, port)
            cur = conn.cursor()
            cur.execute(sql_in)
            columns = [column[0] for column in cur.description]
            for row in cur.fetchall():
                data_out.append(dict(zip(columns, row)))

        except Exception as e:
            # log.exception(e)
            print e
        except RuntimeError as re:
            # log.exception(re)
            print re
        finally:
            conn.close()
        return data_out


class ReconciliationReports(object):

    sql_account = """
      SELECT
       account_details.transaction_number,
       account_details.device_mac,
       account_details.card_type,
       account_details.pkg_name,
       account_details.account_startdate,
       account_details.base_price,
       account_details.gst,
       account_details.pst,
       account_details.hst,
       account_details.purchase_amount,
       account_details.site_uuid,
       account_details.payment_processor,
       account_details.account_date,
       site_details.site_name,
       site_details.dsid,
       site_details.customer_name,
       site_details.customer_code
       FROM
        (select
        COALESCE(account.transaction_number, '') as transaction_number,
        COALESCE(account.device_mac, '') as device_mac,
        COALESCE(account.card_type, '') as card_type,
        COALESCE(account.pkg_name, '') as pkg_name,
        COALESCE(account.account_startdate, '') as account_startdate,
        COALESCE(account.base_price, 0) as base_price,
        COALESCE(account.gst, 0) as gst,
        COALESCE(account.pst, 0) as pst,
        COALESCE(account.hst, 0) as hst,
        COALESCE(account.purchase_amount, 0) as purchase_amount,
        COALESCE(account.site_uuid, '') as site_uuid,
        COALESCE(account.pkg_customerid, 0) as pkg_customerid,
        COALESCE(account.payment_processor, '') as payment_processor,

        to_date(concat(cast(account.year as string),'-', LPAD(cast(account.month as string),2,'0'),'-', LPAD(cast(account.day as string),2,'0')))  as account_date
        from derived.account_details_report account
        where
        to_date(concat(cast(account.year as string),'-', LPAD(cast(account.month as string),2,'0'),'-', LPAD(cast(account.day as string),2,'0')))
        between
        to_date(to_utc_timestamp('{0}', 'Canada/Eastern')) and
        to_date(to_utc_timestamp('{1}', 'Canada/Eastern')))
                                  account_details LEFT JOIN
                                                (select site_uuid, site_name, dsid, customer_name, customer_code from meta.site_details
                                                group by site_uuid, site_name, dsid, customer_name, customer_code
                                                order by customer_name) site_details

        ON account_details.site_uuid = site_details.site_uuid where customer_code = '{2}'
        order by account_startdate;
    """

    sql_fees_ratio = """
    select
    customer.customer_code,
    customer.customer_name,
    customer.fixed_fees,
    customer.revenue_sharing_ratio,
    customer.package_federal_tax,
    customer.package_provincial_tax,
    customer.fees_federal_Tax,
    customer.fees_provincial_tax,
    to_date(concat(cast(customer.year as string),'-', LPAD(cast(customer.month as string),2,'0'),'-', LPAD(cast(customer.day as string),2,'0'))) as update_date
    from meta.customer_details customer
    where
    to_date(concat(cast(customer.year as string),'-', LPAD(cast(customer.month as string),2,'0'),'-', LPAD(cast(customer.day as string),2,'0')))
    between
    to_date(to_utc_timestamp('{0}', 'Canada/Eastern')) and
    to_date(to_utc_timestamp('{1}', 'Canada/Eastern')) and
    customer_code = '{2}';
    """

    sql_rate = """
    select
    concat(credit_card_type_name,'-', payment_process_name,'-', update_date) as pk,
    rate,
    credit_card_type_name,
    payment_process_name,
    update_date
    from meta.creditcardrate
    where
    update_date
    between
    to_date(to_utc_timestamp('{0}', 'Canada/Eastern')) and
    to_date(to_utc_timestamp('{1}', 'Canada/Eastern'))
    """

    fields_details = ('site_name', 'transaction_number', 'device_mac', 'card_type', 'pkg_name', 'account_date', 'net_sales', 'tax_fed', 'tax_prov', 'purchase_amount')

    fields_summary_card = ('dsid', 'site_name', 'card_type', 'total_trans', 'net_sales', 'tax_fed', 'tax_prov', 'gross_sales',
                            'revenue_share_operator',
                            'revenue_share_location',
                            'transaction_charge_variable',
                            'transaction_charge_fixed',
                            'transaction_charge_total',
                            'net_revenue_operator',
                            'net_revenue_location',
                            'rev_share_receivable',
                            'rev_share_payable',
                            'rev_share_summary_net_amount',
                            'trans_fee_payable',
                            'trans_fee_summary_net_amount',
                            'net_sales_receivable',
                            'net_sales_payable',
                            'net_sales_summary_net_amount',
                            'tax_fed_receivable',
                            'tax_fed_payable',
                            'tax_fed_summary_net_amount',
                            'tax_prov_receivable',
                            'tax_prov_payable',
                            'tax_prov_summary_net_amount',
                            'tax_fed_trans_charges',
                            'tax_fed_trans_summary_net_amount',
                            'tax_prov_trans_charges',
                            'tax_prov_trans_summary_net_amount',
                            'total_taxes_receivable',
                            'total_taxes_payable',
                            'total_taxes_trans_summary_net_amount',
                            'total_sale_receivable',
                            'total_sale_payable',
                            'total_sale_summary_net_amount',
                            'to_be_received_receivable',
                            'to_be_received_payable',
                            'to_be_received_summary_net_amount')

    # sql_taxes = "select site_uuid, pst, gst, hst from meta.site_taxes where customer_code = '{0}';"

    def __init__(self, first_date, last_date, customer_code='', customer_name=''):

        self.all_data = []
        self.data_details = []
        self.data_account = []
        self.data_fees_ratio = {}
        self.data_rate = {}

        if first_date:
            self.first_date = datetime.datetime.strptime(first_date, "%Y-%m-%d")
        else:
            self.first_date = datetime.datetime.now().replace(day=1, hour=0, minute=0)

        if last_date:
            self.last_date = datetime.datetime.strptime(last_date, "%Y-%m-%d")
        else:
            current_day = datetime.datetime.now().replace(hour=0, minute=0)
            week_day, last_day = calendar.monthrange(current_day.year, current_day.month)
            self.last_date = datetime.datetime.now().replace(day=last_day, hour=0, minute=0)

        self.customer_code = customer_code
        self.customer_name = customer_name

        self.sql_account = self.sql_account.format(self.first_date, self.last_date, self.customer_code)
        self.sql_fees_ratio = self.sql_fees_ratio.format(self.first_date, self.last_date, self.customer_code)
        self.sql_rate = self.sql_rate.format(self.first_date, self.last_date)

        # self.sql_taxes = self.sql_taxes.format(self.customer_code)

        print self.sql_account
        print self.sql_fees_ratio
        print self.sql_rate

    def get_data_rate(self):
        data_in = get_data_dict_from_hadoop(self.sql_rate)
        data_out = {}
        for data in data_in:
            data_out[data.get('pk', '')] = data.get('rate', '')
        return data_out

    def get_rate_by_card_type_payment_process_date(self, card_type, payment_process, date):
        pk = ''
        if card_type and payment_process and date:
            pk = card_type + '-' + payment_process + '-' + date
        rate = self.data_rate.get(pk, 0)

        if not rate:
            rate = DEFAULT_CREDIT_CARD_RATE.get(card_type, 3.25)

        return rate

    def get_data_fees_ratio(self):
        data_in = get_data_dict_from_hadoop(self.sql_fees_ratio)
        data_out = {}
        for data in data_in:
            data_out[data.get('update_date', '')] = {'fixed_fees': data.get('fixed_fees', ''),
                                                     'revenue_sharing_ratio': data.get('revenue_sharing_ratio', ''),
                                                     'package_federal_tax': data.get('package_federal_tax', ''),
                                                     'package_provincial_tax': data.get('package_provincial_tax', ''),
                                                     'fees_federal_Tax': data.get('fees_federal_Tax', ''),
                                                     'fees_provincial_tax': data.get('fees_provincial_tax', '')
                                                    }
        return data_out

    def get_summary_card(self):
        grouper_card = itemgetter("dsid", "site_name", "card_type")
        result_card = []
        for key, grp in groupby(sorted(self.all_data, key = grouper_card), grouper_card):
            temp_dict = dict(zip(["dsid", "site_name", "card_type"], key))
            grp_list = list(grp)
            temp_dict["total_trans"] = len(grp_list)

            for field in self.fields_summary_card:
                if field not in ("dsid", "site_name", "card_type", "total_trans"):
                    temp_dict[field] = '{0:.2f}'.format(sum([item[field] for item in grp_list]))

            result_card.append(temp_dict)

        return result_card

    def create_files_csv(self):
        #Report data_details
        year = self.last_date.year
        month = self.last_date.month
        path_folder = '{0}/{1}/{2}/{3}/'.format(BASE_SAVE_FOLDER, year, month, self.customer_name.replace(" ", "_"))

        details = self.data_details
        # summary = self.get_summary()
        summary_card = self.get_summary_card()

        if not os.path.exists(path_folder):
            os.makedirs(path_folder)

        if details:
            path_file_details = '{0}{1}'.format(path_folder, 'data_details.csv')
            with open(path_file_details, 'wb') as output_file:
                dict_writer = csv.DictWriter(output_file, self.fields_details)
                dict_writer.writeheader()
                dict_writer.writerows(details)

        # #Report summary
        # if summary:
        #     path_file2 = '{0}{1}'.format(path_folder, 'summary.csv')
        #     keys2 = ('dsid', 'site_name', 'total_trans', 'net_sales', 'taxes', 'gross_sales',
        #              'revenue_share_operator', 'revenue_share_location',
        #              'transaction_charge_variable', 'transaction_charge_fixed', 'transaction_charge_total',
        #              'net_revenue_operator', 'net_revenue_location')
        #     with open(path_file2, 'wb') as output_file:
        #         dict_writer = csv.DictWriter(output_file, keys2)
        #         dict_writer.writeheader()
        #         dict_writer.writerows(summary)

        #Report summary_card
        if summary_card:
            path_file_summary_card = '{0}{1}'.format(path_folder, 'summary_card.csv')
            with open(path_file_summary_card, 'wb') as output_file:
                dict_writer = csv.DictWriter(output_file, self.fields_summary_card)
                dict_writer.writeheader()
                dict_writer.writerows(summary_card)

    def fetch_data(self):
        self.data_account = get_data_dict_from_hadoop(self.sql_account)
        self.data_fees_ratio = self.get_data_fees_ratio()
        self.data_rate = self.get_data_rate()

        # self.data_taxes = self.get_data_taxes()

        print "self.data_fees_ratio", self.data_fees_ratio

        data_details = []

        for account in self.data_account:

            fixed_fees_ratio = self.data_fees_ratio.get(account.get('account_date'), {})

            fixed_fees = fixed_fees_ratio.get('fixed_fees', '')
            ratio = fixed_fees_ratio.get('revenue_sharing_ratio', '')
            package_federal_tax = fixed_fees_ratio.get('package_federal_tax', '')
            package_provincial_tax = fixed_fees_ratio.get('package_provincial_tax', '')
            fees_federal_Tax = fixed_fees_ratio.get('fees_federal_Tax', '')
            fees_provincial_tax = fixed_fees_ratio.get('fees_provincial_tax', '')

            rate = self.get_rate_by_card_type_payment_process_date(account.get('card_type'), account.get('payment_process'), account.get('account_date'))

            # taxes = self.data_taxes.get(account.get('site_uuid'), {})
            # rate_hst = taxes.get('hst', '')
            # rate_gst = taxes.get('gst', '')
            # rate_pst = taxes.get('pst', '')

            if not fixed_fees:
                fixed_fees = FIXED_FEES
            account['fixed_fees'] = fixed_fees

            if not ratio:
                ratio = REVENUE_SHARING_RATIO
            account['ratio'] = ratio

            if not package_federal_tax:
                package_federal_tax = PACKAGE_FEDERAL_TAX
            account['package_federal_tax'] = package_federal_tax

            if not package_provincial_tax:
                package_provincial_tax = PACKAGE_PROVINCIAL_TAX
            account['package_provincial_tax'] = package_provincial_tax

            if not fees_federal_Tax:
                fees_federal_Tax = FEES_FEDERAL_TAX
            account['fees_federal_Tax'] = fees_federal_Tax

            if not fees_provincial_tax:
                fees_provincial_tax = FEES_PROVINCIAL_TAX
            account['fees_provincial_tax'] = fees_provincial_tax

            account['revenue_share_location'] = account['base_price'] * ratio/100
            account['revenue_share_operator'] = account['base_price'] - account['revenue_share_location']

            account['transaction_charge_variable'] = account['purchase_amount'] * rate/100
            account['transaction_charge_fixed'] = fixed_fees
            account['transaction_charge_total'] = account['transaction_charge_variable'] + account['transaction_charge_fixed']

            account['net_revenue_operator'] = account['revenue_share_operator'] + account['transaction_charge_total']
            account['net_revenue_location'] = account['revenue_share_location'] - account['transaction_charge_total']

            # ****************//
            account['net_sales'] = account['base_price']
            account['tax_fed'] = account['gst'] + account['hst']
            account['tax_prov'] = account['pst']
            account["gross_sales"] = account['purchase_amount']

            account['rev_share_receivable']	= account['net_sales']
            account['rev_share_payable'] = account['revenue_share_operator']
            account['rev_share_summary_net_amount'] = account['rev_share_receivable'] - account['rev_share_payable']

            account['trans_fee_payable'] = account['transaction_charge_total']
            account['trans_fee_summary_net_amount'] = 0 - account['trans_fee_payable']

            account['net_sales_receivable'] = account['rev_share_receivable'] - 0
            account['net_sales_payable'] = account['rev_share_payable'] + account['trans_fee_payable']
            account['net_sales_summary_net_amount']	= account['net_sales_receivable'] - account['net_sales_payable']

            account['tax_fed_receivable'] = account['net_sales_receivable'] * package_federal_tax
            account['tax_fed_payable'] = account['rev_share_payable'] * package_federal_tax
            account['tax_fed_summary_net_amount'] = account['tax_fed_receivable'] - account['tax_fed_payable']

            account['tax_prov_receivable'] = account['net_sales_receivable'] * package_provincial_tax
            account['tax_prov_payable'] = account['rev_share_payable'] * package_provincial_tax
            account['tax_prov_summary_net_amount'] = account['tax_prov_receivable'] + account['tax_prov_payable']

            account['tax_fed_trans_charges'] = account['trans_fee_payable'] * fees_federal_Tax

            account['tax_fed_trans_summary_net_amount'] = account['tax_fed_trans_charges'] + 0
            account['tax_prov_trans_charges'] = account['trans_fee_payable'] * fees_provincial_tax
            account['tax_prov_trans_summary_net_amount'] = account['tax_prov_trans_charges'] + 0

            account['total_taxes_receivable'] = account['tax_fed_receivable'] + account['tax_prov_receivable'] + account['tax_fed_trans_charges'] + account['tax_prov_trans_charges']

            account['total_taxes_payable'] = account['tax_fed_payable'] + account['tax_prov_payable']
            account['total_taxes_trans_summary_net_amount'] = account['tax_fed_summary_net_amount'] + account['tax_prov_summary_net_amount'] + account['tax_fed_trans_summary_net_amount'] + account['tax_prov_trans_summary_net_amount']

            account['total_sale_receivable'] = account['net_sales_receivable'] + account['total_taxes_receivable']
            account['total_sale_payable'] = account['net_sales_payable'] + account['tax_fed_payable'] + account['tax_prov_payable']
            account['total_sale_summary_net_amount'] = account['total_sale_receivable'] - account['total_sale_payable']

            account['to_be_received_receivable'] = account['total_sale_receivable'] - 0
            account['to_be_received_payable'] = account['total_sale_payable'] - 0
            account['to_be_received_summary_net_amount'] = account['to_be_received_receivable'] - account['to_be_received_payable']


            #add data
            self.all_data.append(account)

            #Custom data
            data = dict((k, account[k]) for k in self.fields_details)
            self.data_details.append(data)

        # if self.data_details:
        #     total_summary = {'transaction_number': 'total'}
        #     for dictionary in self.data_details:
        #         for key, value in dictionary.items():
        #             if key in ('base_price', 'gst', 'pst', 'hst', 'purchase_amount'):
        #                 if total_summary.has_key(key):
        #                     total_summary[key] = value + total_summary[key]
        #                 else:
        #                     total_summary[key] = value
        #
        #     self.data_details.append(total_summary)

        self.create_files_csv()

        # print 'self.data_taxes = > ', self.data_taxes
        print 'self.data_rate = > ', self.data_rate
        print 'DEFAULT_CREDIT_CARD_RATE => ', DEFAULT_CREDIT_CARD_RATE

        print 'Customer Name : ', self.customer_code, '|', self.customer_name
        print 'FIXED_FEES : ', FIXED_FEES, '|',\
              'REVENUE_SHARING_RATIO : ', REVENUE_SHARING_RATIO, '|',\
              'PACKAGE_FEDERAL_TAX : ', PACKAGE_FEDERAL_TAX, '|',\
              'PACKAGE_PROVINCIAL_TAX : ', PACKAGE_PROVINCIAL_TAX, '|',\
              'FEES_FEDERAL_TAX : ', FEES_FEDERAL_TAX , '|',\
              'FEES_PROVINCIAL_TAX : ', FEES_PROVINCIAL_TAX
        print

        print '-----------------------self.all_data-------------------'
        for a in self.all_data:
            print a

        print '-----------------------data_details-------------------'
        for a in self.data_details:
            print a

        # print '-----------------------summary-------------------'
        # for a in self.get_summary():
        #     print a

        print '-----------------------summary_card-------------------'
        for a in self.get_summary_card():
            print a


if __name__ == "__main__":

    # sql_customer = """
    #       select customer_name, customer_code
    #       from meta.site_details group by customer_name, customer_code
    #       order by customer_name, customer_code
    #     """
    #
    # customers = get_data_dict_from_hadoop(sql_customer)
    #
    # for customer in customers:
    #     print customer.get('customer_code'), '***************',customer.get('customer_name')
    #     rec = ReconciliationReports(first_date='2017-05-13', last_date='2017-05-15', customer_code=customer.get('customer_code'),  customer_name = customer.get('customer_name'))
    #     rec.fetch_data()

    rec = ReconciliationReports(first_date='', last_date='', customer_code='AHI', customer_name='AHI')
    rec = ReconciliationReports(first_date='2017-05-01', last_date='2017-05-30', customer_code='BCS', customer_name='BC Health')
    rec.fetch_data()









# temp_dict['net_sales'] = '{0:.2f}'.format(sum([item["base_price"] for item in grp_list]))
#
# temp_dict["tax_fed"] = '{0:.2f}'.format(sum([item["tax_fed"] for item in grp_list]))
# temp_dict["tax_prov"] = '{0:.2f}'.format(sum([item["tax_prov"] for item in grp_list]))
#
# temp_dict["gross_sales"] = '{0:.2f}'.format(sum([item["purchase_amount"] for item in grp_list]))
# temp_dict["revenue_share_operator"] = '{0:.2f}'.format(sum([item["revenue_share_operator"] for item in grp_list]))
# temp_dict["revenue_share_location"] = '{0:.2f}'.format(sum([item["revenue_share_location"] for item in grp_list]))
# temp_dict["transaction_charge_variable"] = '{0:.2f}'.format(sum([item["transaction_charge_variable"] for item in grp_list]))
# temp_dict["transaction_charge_fixed"] = '{0:.2f}'.format(sum([item["transaction_charge_fixed"] for item in grp_list]))
# temp_dict["transaction_charge_total"] = '{0:.2f}'.format(sum([item["transaction_charge_total"] for item in grp_list]))
# temp_dict["net_revenue_operator"] = '{0:.2f}'.format(sum([item["net_revenue_operator"] for item in grp_list]))
# temp_dict["net_revenue_location"] = '{0:.2f}'.format(sum([item["net_revenue_location"] for item in grp_list]))
#
# temp_dict['rev_share_receivable'] = '{0:.2f}'.format(sum([item["rev_share_receivable"] for item in grp_list]))
# temp_dict['rev_share_payable'] = '{0:.2f}'.format(sum([item["rev_share_payable"] for item in grp_list]))
# temp_dict['rev_share_summary_net_amount'] = '{0:.2f}'.format(sum([item["rev_share_summary_net_amount"] for item in grp_list]))
# temp_dict['trans_fee_payable'] = '{0:.2f}'.format(sum([item["trans_fee_payable"] for item in grp_list]))
# temp_dict['trans_fee_summary_net_amount'] = '{0:.2f}'.format(sum([item["trans_fee_summary_net_amount"] for item in grp_list]))
# temp_dict['net_sales_receivable'] = '{0:.2f}'.format(sum([item["net_sales_receivable"] for item in grp_list]))
# temp_dict['net_sales_payable'] = '{0:.2f}'.format(sum([item["net_sales_payable"] for item in grp_list]))
# temp_dict['net_sales_summary_net_amount'] = '{0:.2f}'.format(sum([item["net_sales_summary_net_amount"] for item in grp_list]))
# temp_dict['tax_fed_receivable'] = '{0:.2f}'.format(sum([item["tax_fed_receivable"] for item in grp_list]))
# temp_dict['tax_fed_payable'] =  '{0:.2f}'.format(sum([item["tax_fed_payable"] for item in grp_list]))
# temp_dict['tax_fed_summary_net_amount'] = '{0:.2f}'.format(sum([item["tax_fed_summary_net_amount"] for item in grp_list]))
# temp_dict['tax_prov_receivable'] = '{0:.2f}'.format(sum([item["tax_prov_receivable"] for item in grp_list]))
# temp_dict['tax_prov_payable'] = '{0:.2f}'.format(sum([item["tax_prov_payable"] for item in grp_list]))
# temp_dict['tax_prov_summary_net_amount'] = '{0:.2f}'.format(sum([item["tax_prov_summary_net_amount"] for item in grp_list]))
# temp_dict['tax_fed_trans_charges'] = '{0:.2f}'.format(sum([item["tax_fed_trans_charges"] for item in grp_list]))
# temp_dict['tax_prov_trans_charges'] = '{0:.2f}'.format(sum([item["tax_prov_trans_charges"] for item in grp_list]))
# temp_dict['total_sale_receivable'] = '{0:.2f}'.format(sum([item["total_sale_receivable"] for item in grp_list]))
# temp_dict['total_sale_payable'] = '{0:.2f}'.format(sum([item["total_sale_payable"] for item in grp_list]))
# temp_dict['total_sale_summary_net_amount'] = '{0:.2f}'.format(sum([item["total_sale_summary_net_amount"] for item in grp_list]))
# temp_dict['to_be_received_receivable'] = '{0:.2f}'.format(sum([item["to_be_received_receivable"] for item in grp_list]))
# temp_dict['to_be_received_payable'] = '{0:.2f}'.format(sum([item["to_be_received_payable"] for item in grp_list]))
# temp_dict['to_be_received_summary_net_amount'] = '{0:.2f}'.format(sum([item["to_be_received_summary_net_amount"] for item in grp_list]))
#
#
# result_card.append(temp_dict)

# if result_card:
#     total_summary = {'dsid': 'total'}
#     for dictionary in result_card:
#         for key, value in dictionary.items():
#             if key not in ("dsid", "site_name", "card_type"):
#                 if total_summary.has_key(key):
#                     total_summary[key] = value + total_summary[key]
#                 else:
#                     total_summary[key] = value
#
#     result_card.append(total_summary)





data = [

  {'account_date': '2018-08-11', 'site_name': 'VANCOUVER GENERAL HOSPITAL', 'tax_fed': 0.5, 'net_sales': 9.95, 'purchase_amount': 11.14, 'card_type': 'Visa', 'transaction_number': '522551-0_132', 'tax_prov': 0.7, 'pkg_name': '24 Hours', 'device_mac': '3451C9D7E890'},
{'account_date': '2018-08-11', 'site_name': 'VANCOUVER GENERAL HOSPITAL', 'tax_fed': 1.45, 'net_sales': 28.95, 'purchase_amount': 32.42, 'card_type': 'Visa', 'transaction_number': '522553-0_132', 'tax_prov': 2.03, 'pkg_name': '1 Week', 'device_mac': '784F437E09AF'},
{'account_date': '2018-08-11', 'site_name': 'VANCOUVER GENERAL HOSPITAL', 'tax_fed': 0.3, 'net_sales': 5.95, 'purchase_amount': 6.66, 'card_type': 'Visa', 'transaction_number': '522557-0_132', 'tax_prov': 0.42, 'pkg_name': '4 Hours', 'device_mac': 'BC8385CC40A6'},
{'account_date': '2018-08-11', 'site_name': 'LIONS GATE HOSPITAL CORE', 'tax_fed': 1.75, 'net_sales': 35.0, 'purchase_amount': 39.2, 'card_type': 'MasterCard', 'transaction_number': '522558-0_132', 'tax_prov': 2.45, 'pkg_name': '30 Days', 'device_mac': 'A4D18C205212'},
{'account_date': '2018-08-12', 'site_name': 'VANCOUVER GENERAL HOSPITAL', 'tax_fed': 0.3, 'net_sales': 5.95, 'purchase_amount': 6.66, 'card_type': 'MasterCard', 'transaction_number': '522576-0_132', 'tax_prov': 0.42, 'pkg_name': '4 Hours', 'device_mac': 'D4A33DF27725'},
{'account_date': '2018-08-12', 'site_name': 'SQUAMISH GENERAL HOSPITAL', 'tax_fed': 0.5, 'net_sales': 9.95, 'purchase_amount': 11.14, 'card_type': 'Visa', 'transaction_number': '522582-0_132', 'tax_prov': 0.7, 'pkg_name': '24 Hours', 'device_mac': '0EEC735DC7AC'},
{'account_date': '2018-08-12', 'site_name': 'SQUAMISH GENERAL HOSPITAL', 'tax_fed': 0.3, 'net_sales': 5.95, 'purchase_amount': 6.66, 'card_type': 'Visa', 'transaction_number': '522583-0_132', 'tax_prov': 0.42, 'pkg_name': '4 Hours', 'device_mac': 'C885500E6DD4'},
{'account_date': '2018-08-12', 'site_name': 'VANCOUVER GENERAL HOSPITAL', 'tax_fed': 0.15, 'net_sales': 2.95, 'purchase_amount': 3.3, 'card_type': 'MasterCard', 'transaction_number': '522588-0_132', 'tax_prov': 0.21, 'pkg_name': '1 Hour', 'device_mac': 'B4749F39EA24'},
{'account_date': '2018-08-12', 'site_name': 'VANCOUVER GENERAL HOSPITAL', 'tax_fed': 0.5, 'net_sales': 9.95, 'purchase_amount': 11.14, 'card_type': 'Visa', 'transaction_number': '522597-0_132', 'tax_prov': 0.7, 'pkg_name': '24 Hours', 'device_mac': 'A402B9806916'},
{'account_date': '2018-08-12', 'site_name': 'VANCOUVER GENERAL HOSPITAL', 'tax_fed': 0.95, 'net_sales': 18.95, 'purchase_amount': 21.22, 'card_type': 'Visa', 'transaction_number': '522616-0_132', 'tax_prov': 1.33, 'pkg_name': '72 Hours', 'device_mac': '6030D40042BD'},
{'account_date': '2018-08-12', 'site_name': 'LIONS GATE HOSPITAL CORE', 'tax_fed': 0.5, 'net_sales': 9.95, 'purchase_amount': 11.14, 'card_type': 'Visa', 'transaction_number': '522623-0_132', 'tax_prov': 0.7, 'pkg_name': '24 Hours', 'device_mac': '784F4371792B'},
{'account_date': '2018-08-12', 'site_name': 'SECHELT GENERAL HOSPITAL CORE', 'tax_fed': 0.95, 'net_sales': 18.95, 'purchase_amount': 21.22, 'card_type': 'Visa', 'transaction_number': '522626-0_132', 'tax_prov': 1.33, 'pkg_name': '72 Hours', 'device_mac': '48605F318681'},
{'account_date': '2018-08-12', 'site_name': 'LIONS GATE HOSPITAL CORE', 'tax_fed': 1.75, 'net_sales': 35.0, 'purchase_amount': 39.2, 'card_type': 'Visa', 'transaction_number': '522648-0_132', 'tax_prov': 2.45, 'pkg_name': '30 Days', 'device_mac': 'A46706095C59'},
{'account_date': '2018-08-12', 'site_name': 'VANCOUVER GENERAL HOSPITAL', 'tax_fed': 0.3, 'net_sales': 5.95, 'purchase_amount': 6.66, 'card_type': 'Visa', 'transaction_number': '522667-0_132', 'tax_prov': 0.42, 'pkg_name': '4 Hours', 'device_mac': 'F85971202411'},
{'account_date': '2018-08-12', 'site_name': 'LIONS GATE HOSPITAL CORE', 'tax_fed': 0.5, 'net_sales': 9.95, 'purchase_amount': 11.14, 'card_type': 'Visa', 'transaction_number': '522675-0_132', 'tax_prov': 0.7, 'pkg_name': '24 Hours', 'device_mac': '1CE62BBB091B'},
{'account_date': '2018-08-13', 'site_name': 'VANCOUVER GENERAL HOSPITAL', 'tax_fed': 0.15, 'net_sales': 2.95, 'purchase_amount': 3.3, 'card_type': 'Visa', 'transaction_number': '522693-0_132', 'tax_prov': 0.21, 'pkg_name': '1 Hour', 'device_mac': '70480FBEDCD7'},
{'account_date': '2018-08-13', 'site_name': 'VANCOUVER GENERAL HOSPITAL', 'tax_fed': 0.15, 'net_sales': 2.95, 'purchase_amount': 3.3, 'card_type': 'Visa', 'transaction_number': '522706-0_132', 'tax_prov': 0.21, 'pkg_name': '1 Hour', 'device_mac': '3C77E668450B'}

]


